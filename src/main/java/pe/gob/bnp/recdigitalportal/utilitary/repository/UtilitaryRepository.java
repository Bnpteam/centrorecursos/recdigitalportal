package pe.gob.bnp.recdigitalportal.utilitary.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;


@Repository
public interface UtilitaryRepository {

	public ResponseTransaction listDepartment();
	
	public ResponseTransaction listProvince(String codeDpt);
	
	public ResponseTransaction listDistrict(String codeDpt,String codeProv);
}
