package pe.gob.bnp.recdigitalportal.utilitary.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

	private TemplateEngine templateEngine;

	@Autowired
	public MailContentBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}
	
	public String buildTemplateRegisterUser(String userFullName,String urlCuenta, String anio) {
		Context context = new Context();
		context.setVariable("NAME", userFullName);
		context.setVariable("URL_CUENTA", urlCuenta);
		context.setVariable("YEAR", anio);
		return templateEngine.process("registerCentroRecursos", context);
	}
	
	public String buildTemplateRecoveryPassword(String userFullName, String urlRecoveryPassword, String anio) {
		Context context = new Context();
		context.setVariable("NAME", userFullName);
		context.setVariable("URL_PASSWORD", urlRecoveryPassword);
		context.setVariable("YEAR", anio);
		return templateEngine.process("recoveryPasswordCentroRecursos", context);
	}
	
	public String buildTemplateEnroll(String userFullName, String urlRecoveryPassword,String course, String anio) {
		Context context = new Context();
		context.setVariable("NAME", userFullName);
		context.setVariable("CURSO", course);
		context.setVariable("URL_ENROLL", urlRecoveryPassword);
		context.setVariable("YEAR", anio);
		return templateEngine.process("enrollCentroRecursos", context);
	}

}
