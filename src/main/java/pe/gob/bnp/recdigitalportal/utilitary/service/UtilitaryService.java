package pe.gob.bnp.recdigitalportal.utilitary.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.repository.UtilitaryRepository;

@Service
public class UtilitaryService {

	@Autowired
	UtilitaryRepository utilitaryRepository;

	public ResponseTransaction searchDepartment() {

		return utilitaryRepository.listDepartment();
	}

	public ResponseTransaction searchProvince(String codeDpt) {

		return utilitaryRepository.listProvince(codeDpt);
	}

	public ResponseTransaction searchDistrict(String codeDpt,String codeProv) {

		return utilitaryRepository.listDistrict(codeDpt,codeProv);
	}

}
