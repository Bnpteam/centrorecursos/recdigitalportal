package pe.gob.bnp.recdigitalportal.utilitary.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class UbigeoResponse {
	
	private String name;
	private String codeDepartment;
	private String codeProvince;
	private String codeDistrict;

	public UbigeoResponse() {
		super();		
		this.name = Constants.EMPTY_STRING;
		this.codeDepartment = Constants.EMPTY_STRING;
		this.codeProvince = Constants.EMPTY_STRING;
		this.codeDistrict = Constants.EMPTY_STRING;
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCodeDepartment() {
		return codeDepartment;
	}


	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}


	public String getCodeProvince() {
		return codeProvince;
	}


	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}


	public String getCodeDistrict() {
		return codeDistrict;
	}


	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}

}