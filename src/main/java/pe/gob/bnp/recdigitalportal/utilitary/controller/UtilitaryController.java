package pe.gob.bnp.recdigitalportal.utilitary.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.dto.UbigeoResponse;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;
import pe.gob.bnp.recdigitalportal.utilitary.service.UtilitaryService;


@RestController
@RequestMapping("/api")
@Api(value = "/api/utility")
@CrossOrigin(origins = "*")
public class UtilitaryController {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilitaryController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	UtilitaryService utilitaryService;
	
	@RequestMapping(method = RequestMethod.GET, path="/departments", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar departamentos", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDepartamentos(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchDepartment();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("departments EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("departments IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("departments Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/provinces/{codeDepartment}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar provincias", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoProvincias(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String codeDepartment){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchProvince(codeDepartment);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("provinces EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("provinces IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("provinces Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}		
		
	}	
			
	
	@RequestMapping(method = RequestMethod.GET, path="/districts/{codeDepartment}/{codeProvince}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar distritos", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDistritos(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String codeDepartment,
			@ApiParam(value = "Código de provincia", required = true)
			@PathVariable String codeProvince){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchDistrict(codeDepartment,codeProvince);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("provinces EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("provinces IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("provinces Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}		
		
	}	

}
