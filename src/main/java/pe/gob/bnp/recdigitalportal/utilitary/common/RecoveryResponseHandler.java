package pe.gob.bnp.recdigitalportal.utilitary.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pe.gob.bnp.recdigitalportal.utilitary.dto.RecoveryResponseTransactionDto;
import pe.gob.bnp.recdigitalportal.utilitary.dto.ResponseDto;
import pe.gob.bnp.recdigitalportal.utilitary.dto.ResponseOkCommandDto;

@Component
public class RecoveryResponseHandler {
	
	private String codeNotFoundResponse="00404";
	private String codeAppCustomErrorResponse="00400";
	private String codeAppExceptionResponse="00500";
	
	public ResponseEntity<Object> getOkCommandResponse(String message) {
		ResponseDto responseDto = new ResponseDto();
		ResponseOkCommandDto responseOkCommandDto = new ResponseOkCommandDto();
		responseOkCommandDto.setHttpStatus(HttpStatus.OK.value());
		responseOkCommandDto.setMessage(message);
		responseDto.setResponse(responseOkCommandDto);
		return new ResponseEntity<Object>(responseDto.getResponse(), HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getOkObjectResponse(Object message) {
		return new ResponseEntity<Object>(message, HttpStatus.OK);
	}
	
	public ResponseEntity<Object> getOkResponseTransaction(RecoveryResponseTransaction responseTx) {
		RecoveryResponseTransactionDto responseTxDto = new RecoveryResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.OK.value());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getAppCustomErrorResponse(RecoveryResponseTransaction responseTx,String errorMessages) {
		
		RecoveryResponseTransactionDto responseTxDto = new RecoveryResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.BAD_REQUEST.value());
		responseTx.setCodeResponse(codeAppCustomErrorResponse);
		responseTx.setResponse(errorMessages);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.BAD_REQUEST);
	}
	
	public ResponseEntity<Object> getAppExceptionResponse(RecoveryResponseTransaction responseTx,Throwable e) {

		RecoveryResponseTransactionDto responseTxDto = new RecoveryResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		responseTx.setCodeResponse(codeAppExceptionResponse);
		responseTx.setResponse("Error Server:"+e.getMessage());
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}		
	
	public ResponseEntity<Object> getNotFoundObjectResponse(RecoveryResponseTransaction responseTx,String message) {
		
		RecoveryResponseTransactionDto responseTxDto = new RecoveryResponseTransactionDto();
		responseTxDto.setHttpStatus(HttpStatus.NOT_FOUND.value());
		responseTx.setCodeResponse(codeNotFoundResponse);
		responseTx.setResponse("Not Found:"+message);
		responseTxDto.setResponse(responseTx);
		return new ResponseEntity<Object>(responseTxDto, HttpStatus.NOT_FOUND);
	}	
}
