package pe.gob.bnp.recdigitalportal.utilitary.common;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value="CourseResponseTransaction", description="transaction response")
public class CourseResponseTransaction {
	
	@ApiModelProperty(notes = "id",required=false,value="100")
	private String id;
	
	@ApiModelProperty(notes = "code response",required=true,value="0000")
	private String codeResponse;
	
	@ApiModelProperty(notes = "description response",required=true,value="Tansacción finalizó con éxito.")
	private String response;
	
	@ApiModelProperty(notes = "list response",required=true,value="No hay registros.")
	private List<Object> list;	
	
	@ApiModelProperty(notes = "topics response",required=true,value="No hay registros.")
	private List<Object> topics;	
	
	@ApiModelProperty(notes = "specialities response",required=true,value="No hay registros.")
	private List<Object>  specialities;	
	

	public CourseResponseTransaction() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.codeResponse = Constants.EMPTY_STRING;
		this.response = Constants.EMPTY_STRING;
		this.list=new ArrayList<Object>();
		this.topics=new ArrayList<Object>();
		this.specialities=new ArrayList<Object>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}

	public List<Object> getTopics() {
		return topics;
	}

	public void setTopics(List<Object> topics) {
		this.topics = topics;
	}

	public List<Object> getSpecialities() {
		return specialities;
	}

	public void setSpecialities(List<Object> specialities) {
		this.specialities = specialities;
	}


}
