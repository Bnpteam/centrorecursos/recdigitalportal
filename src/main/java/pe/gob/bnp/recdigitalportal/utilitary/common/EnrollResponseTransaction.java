package pe.gob.bnp.recdigitalportal.utilitary.common;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="EnrollResponseTransaction", description="transaction response")
public class EnrollResponseTransaction {
	
	@ApiModelProperty(notes = "id",required=false,value="100")
	private String id;
	
	@ApiModelProperty(notes = "code response",required=true,value="0000")
	private String codeResponse;
	
	@ApiModelProperty(notes = "description response",required=true,value="Tansacción finalizó con éxito.")
	private String response;
	
	@ApiModelProperty(notes = "list response",required=true,value="No hay registros.")
	private List<Object> list;	
	
	@ApiModelProperty(notes = "name user",required=true,value="No hay usuario.")
	private String name;		
	
	@ApiModelProperty(notes = "email user",required=true,value="No hay email.")
	private String email;
	
	@ApiModelProperty(notes = "title course",required=true,value="No hay title.")
	private String course;	

	@ApiModelProperty(notes = "link aula virtual",required=true,value="No hay link.")
	private String enrollUrl;	
	

	public EnrollResponseTransaction() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.codeResponse = Constants.EMPTY_STRING;
		this.response = Constants.EMPTY_STRING;
		this.list=new ArrayList<Object>();
		this.name=Constants.EMPTY_STRING;
		this.email=Constants.EMPTY_STRING;
		this.course=Constants.EMPTY_STRING;
		this.enrollUrl=Constants.EMPTY_STRING;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	

	public String getEnrollUrl() {
		return enrollUrl;
	}

	public void setEnrollUrl(String enrollUrl) {
		this.enrollUrl = enrollUrl;
	}
	
	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}
}
