package pe.gob.bnp.recdigitalportal.utilitary.repository;


import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

public interface BaseRepository<T> {
	
	public ResponseTransaction persist(T entity);
	public ResponseTransaction update(T entity);

}
