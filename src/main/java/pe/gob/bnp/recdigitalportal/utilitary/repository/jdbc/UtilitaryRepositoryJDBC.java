package pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.utilitary.dto.UbigeoResponse;
import pe.gob.bnp.recdigitalportal.utilitary.repository.UtilitaryRepository;

@Repository
public class UtilitaryRepositoryJDBC extends BaseJDBCOperation  implements UtilitaryRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UtilitaryRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction listDepartment() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_DPTO(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_DPTO: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTransaction listProvince(String codeDpt) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_PROV(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_CODE_DEPARTMENT", codeDpt);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_PROV: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction listDistrict(String codeDpt,String codeProv) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_DIST(?,?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_CODE_DEPARTMENT", codeDpt);
			cstm.setString("P_CODE_PROVINCE", codeProv);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_DIST: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	
}
