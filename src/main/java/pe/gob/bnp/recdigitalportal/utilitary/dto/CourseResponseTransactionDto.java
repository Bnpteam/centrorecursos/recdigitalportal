package pe.gob.bnp.recdigitalportal.utilitary.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;

public class CourseResponseTransactionDto {
	
	private int httpStatus;
	private CourseResponseTransaction response;
	
	public CourseResponseTransaction getResponse() {
		return response;
	}

	public void setResponse(CourseResponseTransaction response) {
		this.response = response;
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	

}
