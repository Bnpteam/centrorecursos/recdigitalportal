package pe.gob.bnp.recdigitalportal.utilitary.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;

public class EnrollResponseTransactionDto {
	
	private int httpStatus;
	private EnrollResponseTransaction response;

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public EnrollResponseTransaction getResponse() {
		return response;
	}

	public void setResponse(EnrollResponseTransaction response) {
		this.response = response;
	}

}
