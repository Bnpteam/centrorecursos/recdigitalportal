package pe.gob.bnp.recdigitalportal.utilitary.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;

public class ResourceResponseTransactionDto {
	
	private int httpStatus;
	private ResourceResponseTransaction response;
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ResourceResponseTransaction getResponse() {
		return response;
	}

	public void setResponse(ResourceResponseTransaction response) {
		this.response = response;
	}

}
