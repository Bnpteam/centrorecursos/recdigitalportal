package pe.gob.bnp.recdigitalportal.utilitary.common;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Utility {
	
	private static final String FORMAT_DATE = "dd/MM/yyyy";
	private static final String FORMAT_DATE_TWO = "yyyy-MM-dd";

	/* Dates */

	public static String parseDateToString(Date date) {
		String resultado = Constants.EMPTY_STRING;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		resultado = dateFormat.format(date);
		return resultado;
	}
	
	

	public static Date parseStringToDate(String date) {
		Date resultado = null;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return resultado;
	}
	
	public static String parseDateToStringTwo(Date date) {
		String resultado = Constants.EMPTY_STRING;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		resultado = dateFormat.format(date);
		return resultado;
	}
	
	public static Date parseStringToDateTwo(String date) {
		Date resultado = null;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return resultado;
	}

	public static Date getDate(java.sql.Date sqlDate) {
		if (sqlDate == null)
			return null;
		return new Date(sqlDate.getTime());
	}

	public static java.sql.Date getSQLDate(Date date) {
		if (date == null)
			return null;
		return new java.sql.Date(date.getTime());
	}

	public static String parseSqlDateToString(java.sql.Date sqlDate) {
		Date date = getDate(sqlDate);
		return parseDateToString(date);
	}
	
	public static String parseSqlDateToStringTwo(java.sql.Date sqlDate) {
		Date date = getDate(sqlDate);
		return parseDateToStringTwo(date);
	}

	public static java.sql.Date parseStringToSQLDate(String date) {
		Date resultado = null;
		if (date == null)
			return null;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return getSQLDate(resultado);
	}
	
	public static java.sql.Date parseStringToSQLDateTwo(String date) {
		Date resultado = null;
		if (date == null)
			return null;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return getSQLDate(resultado);
	}

	/* Long */

	public static Long parseObjectToLong(Object objeto) {
		BigDecimal numeroBD = new BigDecimal(Constants.ZERO_LONG);
		numeroBD = (BigDecimal) objeto;
		Long id = numeroBD.longValue();
		return id;
	}

	public static Long parseStringToLong(String numero) {
		Long resultado;
		if (numero == null || numero.trim().equals(Constants.EMPTY_STRING))
			return Constants.ZERO_LONG;
		resultado = Long.parseLong(numero);
		return resultado;
	}

	public static String parseLongToString(Long numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_LONG)
			return Constants.EMPTY_STRING;
		resultado = String.valueOf(numero);
		return resultado;
	}

	/* Integer */

	public static Integer getInteger(Integer obj) {
		if (obj == null) {
			return Constants.ZERO_INTEGER;
		} else {
			return obj;
		}
	}

	public static String parseIntToString(Integer numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_INTEGER)
			return Constants.EMPTY_STRING;
		resultado = String.valueOf(numero);
		return resultado;
	}

	public static String parseIntToString2(Integer numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_INTEGER)
			return "0";
		resultado = String.valueOf(numero);
		return resultado;
	}

	public static Integer parseStringToInt(String numero) {
		int resultado;
		if (numero == null || numero.trim().equals(Constants.EMPTY_STRING))
			return Constants.ZERO_INTEGER;
		resultado = Integer.parseInt(numero);
		return resultado;
	}

	/* Empty */

	public static boolean isEmptyList(List<?> obj) {
		if ((obj == null) || (obj.size() == Constants.ZERO_INTEGER)) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(String string) {
		return string == null || string.trim().isEmpty();
	}

	public static boolean isEmptyOrNull(String string) {
		return (isEmpty(string) || (string.trim().equalsIgnoreCase(Constants.NULL_STRING)));
	}

	public static String trimString(String string) {
		return isEmptyOrNull(string) ? Constants.EMPTY_STRING : string.trim();
	}

	public static boolean isEquals(String obj1, String obj2) {
		if (obj1.equalsIgnoreCase(obj2)) {
			return true;
		}
		return false;
	}

	public static String getString(String obj) {
		if (obj == null) {
			return Constants.EMPTY_STRING;
		} else {
			return obj;
		}
	}

	/*
	 * Files
	 * 
	 */
	public static Boolean createFolder(String folderName) {
		Boolean result = false;
		try {
			File file = new File(folderName);
			if (!file.exists()) {
				file.mkdir();
				result = true;
			} else {
				result = true;
			}
		} catch (SecurityException sex) {
			result = false;
			sex.getMessage();
		} catch (Exception e) {
			result = false;
			e.getMessage();
		}
		return result;
	}

	/*
	 * generate random
	 * 
	 */

	public static String generateRandomString(int length) {
		int leftLimit = 48; // numeral '0'
		int rightLimit = 122; // letter 'z'	
		Random random = new Random();

		String generatedString = random.ints(leftLimit, rightLimit + 1)
				.filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(length)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		
		return generatedString;
	}

	public static String generateRandomStringByUUIDNoDash() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
