package pe.gob.bnp.recdigitalportal.utilitary.common;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="ResourceResponseTransaction", description="transaction response")
public class ResourceResponseTransaction {
	
	@ApiModelProperty(notes = "id",required=false,value="100")
	private String id;
	
	@ApiModelProperty(notes = "code response",required=true,value="0000")
	private String codeResponse;
	
	@ApiModelProperty(notes = "description response",required=true,value="Tansacción finalizó con éxito.")
	private String response;
	
	@ApiModelProperty(notes = "list response",required=true,value="No hay registros.")
	private List<Object> list;	
	
	@ApiModelProperty(notes = "categories response",required=true,value="No hay registros.")
	private List<Object> categories;	
	
	//@ApiModelProperty(notes = "formats response",required=true,value="No hay registros.")
	//private List<Object>  formats;	
	
	
	public ResourceResponseTransaction() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.codeResponse = Constants.EMPTY_STRING;
		this.response = Constants.EMPTY_STRING;
		this.list=new ArrayList<Object>();
		this.categories=new ArrayList<Object>();
		//this.formats=new ArrayList<Object>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodeResponse() {
		return codeResponse;
	}

	public void setCodeResponse(String codeResponse) {
		this.codeResponse = codeResponse;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}

	public List<Object> getCategories() {
		return categories;
	}

	public void setCategories(List<Object> categories) {
		this.categories = categories;
	}

/*	public List<Object> getFormats() {
		return formats;
	}

	public void setFormats(List<Object> formats) {
		this.formats = formats;
	}
*/

}
