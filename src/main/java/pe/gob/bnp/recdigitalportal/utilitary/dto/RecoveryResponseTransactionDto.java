package pe.gob.bnp.recdigitalportal.utilitary.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;

public class RecoveryResponseTransactionDto {

	private int httpStatus;
	private RecoveryResponseTransaction response;
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public RecoveryResponseTransaction getResponse() {
		return response;
	}

	public void setResponse(RecoveryResponseTransaction response) {
		this.response = response;
	}
}
