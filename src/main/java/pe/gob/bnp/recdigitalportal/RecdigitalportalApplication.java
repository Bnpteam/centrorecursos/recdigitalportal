package pe.gob.bnp.recdigitalportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecdigitalportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecdigitalportalApplication.class, args);
	}

}
