package pe.gob.bnp.recdigitalportal.start.dto;

public class CourseEnrollResponse {
	
	private String id;	
	private String title;
	private String description;
	//private String categoryId;
	//private String subcategoryId;
	//private String category;
	private String topicId;
	private String topic;
	private String specialityId;
	private String speciality;	
	private String linkCar;
	private String views;
	private String rating;
	private String publicationDate;		
	private String totalRecords;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getSpecialityId() {
		return specialityId;
	}
	public void setSpecialityId(String specialityId) {
		this.specialityId = specialityId;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	public String getLinkCar() {
		return linkCar;
	}
	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	public String getViews() {
		return views;
	}
	public void setViews(String views) {
		this.views = views;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}	
	
	
	
}
