package pe.gob.bnp.recdigitalportal.start.dto;

public class ResourcePublishedResponse {
	
	private String id;
	private String titleOriginal;
	private String title;
	private String description;
	private String titleBig;
	private String descriptionBig;
	private String categoryId;
	private String category;	
	private String formatId;
	private String format;
	//private String formatIcon;	
	//private String icon;	
	//private String subcategoryId;
	//private String subcategory;
	private String linkCar;
	private String linkCar2;
	private String linkCar3;	
	private String link;
	private String observation;	
	private String views;
	private String likes;
	private String shares;
	private String publicationDate;	
	private String totalRecords;
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getTitleOriginal() {
		return titleOriginal;
	}
	public void setTitleOriginal(String titleOriginal) {
		this.titleOriginal = titleOriginal;
	}
	
	/*public String getFormatIcon() {
		return formatIcon;
	}
	public void setFormatIcon(String formatIcon) {
		this.formatIcon = formatIcon;
	}*/
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	/*public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	*/
	public String getLinkCar() {
		return linkCar;
	}
	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	
	public String getLinkCar2() {
		return linkCar2;
	}
	public void setLinkCar2(String linkCar2) {
		this.linkCar2 = linkCar2;
	}
	public String getLinkCar3() {
		return linkCar3;
	}
	public void setLinkCar3(String linkCar3) {
		this.linkCar3 = linkCar3;
	}
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getViews() {
		return views;
	}
	public void setViews(String views) {
		this.views = views;
	}
	public String getLikes() {
		return likes;
	}
	public void setLikes(String likes) {
		this.likes = likes;
	}
	public String getShares() {
		return shares;
	}
	public void setShares(String shares) {
		this.shares = shares;
	}

	public String getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	
	public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getTitleBig() {
		return titleBig;
	}
	public void setTitleBig(String titleBig) {
		this.titleBig = titleBig;
	}
	public String getDescriptionBig() {
		return descriptionBig;
	}
	public void setDescriptionBig(String descriptionBig) {
		this.descriptionBig = descriptionBig;
	}
}
