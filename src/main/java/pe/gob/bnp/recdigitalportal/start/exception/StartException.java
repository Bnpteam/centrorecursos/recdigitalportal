package pe.gob.bnp.recdigitalportal.start.exception;

import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

public class StartException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El id del recurso no esta publicado o no existe";
	public static final  String error0002CourseList="El tipo de order indicado no existe";
	public static final String error0003ResourceList="La categoria id no existe";

	public static ResponseTransaction setMessageResponseSaveLink(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		return response;
	}
	
	
	public static ResponseTransaction setMessageResponseCourseList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002CourseList);
		}
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseResourceList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002CourseList);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003ResourceList);
		}
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseCourseListEnroll(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
			
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseResourceListEnroll(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
			
		
		return response;
	}

}
