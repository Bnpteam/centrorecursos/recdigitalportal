package pe.gob.bnp.recdigitalportal.start.dto;

public class CourseRequest {
	
	private String order;
	
	private String page;
	
	private String size;

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

}
