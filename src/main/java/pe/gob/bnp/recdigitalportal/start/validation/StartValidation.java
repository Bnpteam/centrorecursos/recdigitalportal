package pe.gob.bnp.recdigitalportal.start.validation;

import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.start.dto.CalificationDto;
import pe.gob.bnp.recdigitalportal.start.dto.CourseRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareDto;
import pe.gob.bnp.recdigitalportal.utilitary.common.Notification;

public class StartValidation {
	
	private static String messageUserLogin = "No se encontraron datos del recurso";
	private static String messageDomainSave = "Se debe ingresar un id recurso";
	private static String messageCalificationScore= "Se debe ingresar un score";
	
	private static String messageCourseList = "No se encontraron datos de order, page o size";
	private static String messageCourseListOrder= "Se debe ingresar un tipo de order";
//	private static String messageCourseListCategory= "Se debe ingresar id categoria";
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";
	
	public static Notification validation(ResourceDto resourceDto) {
		Notification notification = new Notification();

		if (resourceDto == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(resourceDto.getId())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}	
	
	public static Notification validation(ResourceShareDto resourceDto) {
		Notification notification = new Notification();

		if (resourceDto == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(resourceDto.getId())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}	
	
	public static Notification validation(CalificationDto calificationDto) {
		Notification notification = new Notification();

		if (calificationDto == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(calificationDto.getId())) {
			notification.addError(messageDomainSave);
		}
		
		if (Utility.isEmptyOrNull(calificationDto.getScore())) {
			notification.addError(messageCalificationScore);
		}

		return notification;
	}	
	
	

	public static Notification validation(CourseRequest courseRequest) {
		Notification notification = new Notification();

		if (courseRequest == null) {
			notification.addError(messageCourseList);
			return notification;
		}

		if (Utility.isEmptyOrNull(courseRequest.getOrder())) {
			notification.addError(messageCourseListOrder);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getSize())) {
			notification.addError(messageCourseListSize);
		}

		return notification;
	}
	
	public static Notification validation(ResourceRequest resourceRequest) {
		Notification notification = new Notification();

		if (resourceRequest == null) {
			notification.addError(messageCourseList);
			return notification;
		}

		if (Utility.isEmptyOrNull(resourceRequest.getOrder())) {
			notification.addError(messageCourseListOrder);
		}
		
		/*if (Utility.isEmptyOrNull(resourceRequest.getCategoryId())) {
			notification.addError(messageCourseListCategory);
		}*/
		
		if (Utility.isEmptyOrNull(resourceRequest.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(resourceRequest.getSize())) {
			notification.addError(messageCourseListSize);
		}

		return notification;
	}

}
