package pe.gob.bnp.recdigitalportal.start.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigitalportal.start.dto.ButtonDetailResponse;
import pe.gob.bnp.recdigitalportal.start.dto.CalificationDto;
import pe.gob.bnp.recdigitalportal.start.dto.CourseEnrollResponse;
import pe.gob.bnp.recdigitalportal.start.dto.CoursePublishedResponse;
import pe.gob.bnp.recdigitalportal.start.dto.CourseRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDownloadResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceEnrollResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceLikeResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourcePublishedResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRatingResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareResponse;
import pe.gob.bnp.recdigitalportal.start.dto.SliderDetailResponse;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc.BaseJDBCOperation;
import pe.gob.bnp.recdigitalportal.start.repository.StartRepository;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

@Repository
public class StartRepositoryJDBC extends BaseJDBCOperation  implements StartRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(StartRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction list() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_SLIDER(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					SliderDetailResponse sliderResponse = new SliderDetailResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("SLIDER_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("SLIDER_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("SLIDER_SUBTITLE")));
					sliderResponse.setButtons(this.readList(rs.getLong("SLIDER_ID")));
					slidersResponse.add(sliderResponse);
				}	
				response.setList(slidersResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_LIST_SLIDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	private List<ButtonDetailResponse> readList(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_GET_BUTTON(?,?,?)}";
       // SliderDetailResponse sliderResponse = new SliderDetailResponse();
       // List<Object> slidersResponse = new ArrayList<>();
        List<ButtonDetailResponse> buttonDto = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
        
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_SLIDER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);	
			cstm.registerOutParameter("S_C_CURSOR_BUTTON",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);				
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_BUTTON");			
				while (rs.next()) {
					ButtonDetailResponse entidad = new ButtonDetailResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("BUTTON_ID")));								
					entidad.setName(Utility.getString(rs.getString("BUTTON_NAME")));
					entidad.setUrl(Utility.getString(rs.getString("BUTTON_URL")));									
					buttonDto.add(entidad);
				}	
				//sliderResponse.setButtons(buttonDto);			
			
			}
		}catch(SQLException e) {
			logger.error("SP_GET_BUTTON: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return buttonDto;	
	}
	
	
	@Override
	public ResponseTransaction persistDownload(ResourceDto resourceDto) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_PERSIST_DOWNLOAD(?,?,?,?)}";
       
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID",Utility.parseStringToLong(resourceDto.getId()));
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(resourceDto.getCreatedUser()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceDownloadResponse resourceResponse = new ResourceDownloadResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));				
					subscriptionsResponse.add(resourceResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_START.SP_PERSIST_DOWNLOAD: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persistLike(ResourceDto resourceDto) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_PERSIST_LIKE(?,?,?,?)}";
       
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID",Utility.parseStringToLong(resourceDto.getId()));
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(resourceDto.getCreatedUser()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceLikeResponse resourceResponse = new ResourceLikeResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));	
					resourceResponse.setLikes(Utility.getString(rs.getString("NUMBER_LIKES")));	
					resourceResponse.setFlagLike(Utility.getString(rs.getString("LIKES")));
					subscriptionsResponse.add(resourceResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			logger.error("PKG_API_START.SP_PERSIST_LIKE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persistShare(ResourceShareDto resourceDto) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_PERSIST_SHARE(?,?,?,?,?,?)}";
       
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID",Utility.parseStringToLong(resourceDto.getId()));
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(resourceDto.getCreatedUser()));
			cstm.setString("P_SHARE_TYPE",Utility.getString(resourceDto.getShareType()));
			cstm.setString("P_SHARE_LINK", Utility.getString(resourceDto.getShareLink()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceShareResponse resourceResponse = new ResourceShareResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));				
					subscriptionsResponse.add(resourceResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			logger.error("SP_PERSIST_SHARE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction list(CourseRequest courseRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_COURSE(?,?,?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setInt("P_ORDER", Utility.parseStringToInt(courseRequest.getOrder()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(courseRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(courseRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					CoursePublishedResponse resourceResponse = new CoursePublishedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("COURSE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("COURSE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("COURSE_DESCRIPTION")));
					//resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));
					//resourceResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));
					//resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//resourceResponse.setSubcategory(Utility.getString(rs.getString("RESOURCE_SUBCATEGORY")));
					resourceResponse.setTopicId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					resourceResponse.setTopic(Utility.getString(rs.getString("TOPIC_NAME")));
					resourceResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					resourceResponse.setSpeciality(Utility.getString(rs.getString("SPECIALITY_NAME")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setRating(Utility.getString(rs.getString("RATING")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_START.SP_LIST_COURSE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction list(ResourceRequest resourceRequest) {
		
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_RESOURCE(?,?,?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setInt("P_ORDER", Utility.parseStringToInt(resourceRequest.getOrder()));	
			//cstm.setInt("P_CATEGORY_ID", Utility.parseStringToInt(resourceRequest.getCategoryId()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(resourceRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(resourceRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					ResourcePublishedResponse resourceResponse = new ResourcePublishedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitleOriginal(Utility.getString(rs.getString("RESOURCE_TITLE_ORIGINAL")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setTitleBig(Utility.getString(rs.getString("RESOURCE_TITLE_BIG")));
					resourceResponse.setDescriptionBig(Utility.getString(rs.getString("RESOURCE_DESCRIPTION_BIG")));
					//resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));
					//resourceResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));
					//resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//resourceResponse.setSubcategory(Utility.getString(rs.getString("RESOURCE_SUBCATEGORY")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("CATEGORY_NAME")));
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("FORMAT_NAME")));	
					//resourceResponse.setFormatIcon(Utility.getString(rs.getString("FORMAT_ICON")));	
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK3")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setObservation(Utility.getString(rs.getString("RESOURCE_OBSERVATION")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setLikes(Utility.getString(rs.getString("NUMBER_LIKES")));
					resourceResponse.setShares(Utility.getString(rs.getString("NUMBER_SHARES")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_START.SP_LIST_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction persistRating(CalificationDto calificationDto) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_PERSIST_RATING(?,?,?,?,?)}";
       
        List<Object> subscriptionsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID",Utility.parseStringToLong(calificationDto.getId()));
			cstm.setLong("P_SCORE",Utility.parseStringToLong(calificationDto.getScore()));
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(calificationDto.getCreatedUser()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResourceRatingResponse resourceResponse = new ResourceRatingResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));	
					resourceResponse.setRating(Utility.getString(rs.getString("RATING")));
					subscriptionsResponse.add(resourceResponse);
				}	
				response.setList(subscriptionsResponse);					
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_START.SP_PERSIST_RATING: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction searchcoursesEnroll(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_COURSE_ENROLL(?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_USER_ID", id);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					CourseEnrollResponse resourceResponse = new CourseEnrollResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("COURSE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("COURSE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("COURSE_DESCRIPTION")));
					//resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));					
					//resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setTopicId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					resourceResponse.setTopic(Utility.getString(rs.getString("TOPIC_NAME")));
					resourceResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					resourceResponse.setSpeciality(Utility.getString(rs.getString("SPECIALITY_NAME")));
					
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setRating(Utility.getString(rs.getString("RATING")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));		
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_START.SP_LIST_COURSE_ENROLL: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTransaction searchresourcesEnroll(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_RESOURCE_ENROLL(?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_USER_ID", id);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					ResourceEnrollResponse resourceResponse = new ResourceEnrollResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitleOriginal(Utility.getString(rs.getString("RESOURCE_TITLE_ORIGINAL")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setTitleBig(Utility.getString(rs.getString("RESOURCE_TITLE_BIG")));
					resourceResponse.setDescriptionBig(Utility.getString(rs.getString("RESOURCE_DESCRIPTION_BIG")));
					//resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));					
					//resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//resourceResponse.setCategory(Utility.getString(rs.getString("RESOURCE_CATEGORY")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("CATEGORY_NAME")));
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("FORMAT_NAME")));
					//resourceResponse.setFormatIcon(Utility.getString(rs.getString("FORMAT_ICON")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setObservation(Utility.getString(rs.getString("RESOURCE_OBSERVATION")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setLikes(Utility.getString(rs.getString("NUMBER_LIKES")));
					resourceResponse.setShares(Utility.getString(rs.getString("NUMBER_SHARES")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_LIST_RESOURCE_ENROLL: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
