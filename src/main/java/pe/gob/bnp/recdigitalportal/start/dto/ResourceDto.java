package pe.gob.bnp.recdigitalportal.start.dto;

public class ResourceDto {
	
	private String id;
	private String createdUser;
	
	public ResourceDto() {
		super();
		this.id = "";	
		this.createdUser = "";		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	
	

}
