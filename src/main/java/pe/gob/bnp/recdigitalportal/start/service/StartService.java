package pe.gob.bnp.recdigitalportal.start.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigitalportal.utilitary.common.Notification;
import pe.gob.bnp.recdigitalportal.start.dto.CalificationDto;
import pe.gob.bnp.recdigitalportal.start.dto.CourseRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareDto;
import pe.gob.bnp.recdigitalportal.start.exception.StartException;
import pe.gob.bnp.recdigitalportal.start.repository.StartRepository;
import pe.gob.bnp.recdigitalportal.start.validation.StartValidation;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;


@Service
public class StartService {

	@Autowired
	StartRepository startRepository;
	
	public ResponseTransaction searchAll(){	
		
		return startRepository.list();
	}
	
	public ResponseTransaction searchcoursesEnroll(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = startRepository.searchcoursesEnroll(id);

		return StartException.setMessageResponseCourseListEnroll(response);
	}
	
	public ResponseTransaction searchresourcesEnroll(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = startRepository.searchresourcesEnroll(id);

		return StartException.setMessageResponseResourceListEnroll(response);
	}
	
	
	public ResponseTransaction searchCourseAll(String order, String page, String size){	

		ResponseTransaction response = new ResponseTransaction();
		
		CourseRequest courseRequest = new CourseRequest();
		courseRequest.setOrder(order);
		courseRequest.setPage(page);
		courseRequest.setSize(size);
		
		Notification notification = StartValidation.validation(courseRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			System.out.println("0077");
			return response;
		}	
		
		response=startRepository.list(courseRequest);
		
		return StartException.setMessageResponseCourseList(response);	
	}
	
	public ResponseTransaction searchResourceAll(String order, String page, String size){	

		ResponseTransaction response = new ResponseTransaction();
		
		ResourceRequest resourceRequest = new ResourceRequest();
		resourceRequest.setOrder(order);
	//	resourceRequest.setCategoryId(categoryId);
		resourceRequest.setPage(page);
		resourceRequest.setSize(size);
		
		Notification notification = StartValidation.validation(resourceRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());			
			return response;
		}	
		
		response=startRepository.list(resourceRequest);
		
		return StartException.setMessageResponseResourceList(response);	
	}
	
	/*public ResponseTransaction persistView(ResourceDto resourceDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = StartValidation.validation(resourceDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=startRepository.persistView(resourceDto);
		
		return StartException.setMessageResponseSaveLink(response);	
	}*/
	
	public ResponseTransaction persistDownload(ResourceDto resourceDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = StartValidation.validation(resourceDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=startRepository.persistDownload(resourceDto);
		
		return StartException.setMessageResponseSaveLink(response);	
	}

	public ResponseTransaction persistLike(ResourceDto resourceDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = StartValidation.validation(resourceDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=startRepository.persistLike(resourceDto);
		
		return StartException.setMessageResponseSaveLink(response);	
	}
	
	public ResponseTransaction persistShare(ResourceShareDto resourceDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = StartValidation.validation(resourceDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=startRepository.persistShare(resourceDto);
		
		return StartException.setMessageResponseSaveLink(response);	
	}

	
	public ResponseTransaction persistRating(CalificationDto calificationDto) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = StartValidation.validation(calificationDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		response=startRepository.persistRating(calificationDto);
		
		return StartException.setMessageResponseSaveLink(response);	
	}
}
