package pe.gob.bnp.recdigitalportal.start.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigitalportal.start.dto.CalificationDto;
import pe.gob.bnp.recdigitalportal.start.dto.CourseRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRequest;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareDto;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

@Repository
public interface StartRepository {
	
	public ResponseTransaction list();
	
	public ResponseTransaction list(CourseRequest courseRequest);
	
	public ResponseTransaction list(ResourceRequest resourceRequest);
	
	public ResponseTransaction persistDownload(ResourceDto resourceDto);
	
	public ResponseTransaction persistLike(ResourceDto resourceDto);
	
	public ResponseTransaction persistShare(ResourceShareDto resourceDto);
	
	public ResponseTransaction persistRating(CalificationDto calificationDto);
	
	public ResponseTransaction searchcoursesEnroll(Long id);
	
	public ResponseTransaction searchresourcesEnroll(Long id);
}
