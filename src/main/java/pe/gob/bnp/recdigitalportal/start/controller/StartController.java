package pe.gob.bnp.recdigitalportal.start.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.start.dto.CalificationDto;
//import pe.gob.bnp.recdigitalportal.start.dto.CourseEnrollResponse;
//import pe.gob.bnp.recdigitalportal.start.dto.CoursePublishedResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDownloadResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceEnrollResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceLikeResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourcePublishedResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceRatingResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareDto;
import pe.gob.bnp.recdigitalportal.start.dto.ResourceShareResponse;
import pe.gob.bnp.recdigitalportal.start.dto.SliderDetailResponse;
import pe.gob.bnp.recdigitalportal.start.service.StartService;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value = "/api/start")
@CrossOrigin(origins = "*")
public class StartController {
	
	private static final Logger logger = LoggerFactory.getLogger(StartController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	StartService startService;

	@RequestMapping(method = RequestMethod.GET, path = "/sliders", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar sliders activos", response = SliderDetailResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchAll() {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.searchAll();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("sliders EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("sliders IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("sliders Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	/*@RequestMapping(method = RequestMethod.GET, path = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar cursos publicados", response = CoursePublishedResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchCourseAll(	
			@RequestParam(value = "order", defaultValue = "", required=true) String  order,
			@RequestParam(value = "page",  defaultValue = "", required=true) String  page,
			@RequestParam(value = "size",  defaultValue = "", required=true) String  size){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.searchCourseAll(order,page,size);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				logger.info("courses order:"+order+" page: "+page+" size: "+size+" No se encontraron registros. ");
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("courses order:"+order+" page: "+page+" size: "+size+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("courses order:"+order+" page: "+page+" size: "+size+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("courses order:"+order+" page: "+page+" size: "+size+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}*/
	
	@RequestMapping(method = RequestMethod.GET, path = "/resources", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar recursos publicados", response = ResourcePublishedResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchResourceAll(	
			@RequestParam(value = "order", defaultValue = "", required=true) String  order,
			//@RequestParam(value = "categoryId", defaultValue = "", required=true) String  categoryId,
			@RequestParam(value = "page",  defaultValue = "", required=true) String  page,
			@RequestParam(value = "size",  defaultValue = "", required=true) String  size){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.searchResourceAll(order,page,size);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				logger.info("resources order:"+order+" page: "+page+" size: "+size+" No se encontraron registros. ");
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("resources order:"+order+" page: "+page+" size: "+size+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("resources order:"+order+" page: "+page+" size: "+size+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("resources order:"+order+" page: "+page+" size: "+size+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	 //saveDownload
	@RequestMapping(method = RequestMethod.POST, path = "/savedownload", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar una descarga", response = ResourceDownloadResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> saveView(
			@ApiParam(value = "objeto contiene id, usuario que registra una descarga", required = true) @RequestBody ResourceDto resourceDto) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.persistDownload(resourceDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("saveview IllegalArgumentException:"+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("saveview Throwable:"+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	// saveLike
	@RequestMapping(method = RequestMethod.POST, path = "/savelike", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar un like", response = ResourceLikeResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> saveLike(
			@ApiParam(value = "objeto contiene id, usuario que registra un like", required = true) @RequestBody ResourceDto resourceDto) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.persistLike(resourceDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("savelike IllegalArgumentException:"+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("savelike Throwable:"+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	// saveShare
	@RequestMapping(method = RequestMethod.POST, path = "/saveshare", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar un compartir", response = ResourceShareResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
	@ApiResponse(code = 400, message = "Solicitud contiene errores"),
	@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> saveShare(
				@ApiParam(value = "objeto contiene id recurso, usuario, link y tipo que registra un compartir", required = true) @RequestBody ResourceShareDto resourceDto) {
			ResponseTransaction response = new ResponseTransaction();
			try {
				response = startService.persistShare(resourceDto);
				return this.responseHandler.getOkResponseTransaction(response);
			} catch (IllegalArgumentException ex) {
				logger.error("saveshare IllegalArgumentException:"+ex.getMessage());
				return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
			} catch (Throwable ex) {
				logger.error("saveshare Throwable:"+ex.getMessage());
				return this.responseHandler.getAppExceptionResponse(response, ex);
			}

		}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/saverating", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar una calificacion", response = ResourceRatingResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> saveRating(
			@ApiParam(value = "objeto contiene id, usuario que registra una calificacion", required = true) @RequestBody CalificationDto calificationDto) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.persistRating(calificationDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("saverating IllegalArgumentException:"+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("saverating Throwable:"+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	/*@RequestMapping(method = RequestMethod.GET, path = "/resourcesview", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar sliders activos", response = SliderDetailResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> resourcesview() {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = startService.searchAll();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("sliders EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("sliders IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("sliders Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}*/
	
	/*
	 * enroll
	 */
	
	/*@RequestMapping(method = RequestMethod.GET, path="/coursesenroll/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "cursos matriculados BNP", response= CourseEnrollResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getcoursesEnroll(
			@ApiParam(value = "id alumno", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = startService.searchcoursesEnroll(id);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("coursesenroll/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("coursesenroll/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("coursesenroll/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}*/
	
	@RequestMapping(method = RequestMethod.GET, path="/resourcesenroll/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "recursos vistos BNP", response= ResourceEnrollResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getresourcesEnroll(
			@ApiParam(value = "id alumno", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = startService.searchresourcesEnroll(id);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("resourcesenroll/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("resourcesenroll/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("resourcesenroll/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

}
