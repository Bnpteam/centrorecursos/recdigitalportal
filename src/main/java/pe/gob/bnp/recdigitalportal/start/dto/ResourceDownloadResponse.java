package pe.gob.bnp.recdigitalportal.start.dto;

public class ResourceDownloadResponse {
	
	private String id;

	private String link;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
