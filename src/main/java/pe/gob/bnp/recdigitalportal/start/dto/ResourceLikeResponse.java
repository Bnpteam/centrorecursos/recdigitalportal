package pe.gob.bnp.recdigitalportal.start.dto;

public class ResourceLikeResponse {
	
	private String id;
	
	private String link;
	
	private String likes;
	
	private String flagLike;

	public String getFlagLike() {
		return flagLike;
	}

	public void setFlagLike(String flagLike) {
		this.flagLike = flagLike;
	}

	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


}
