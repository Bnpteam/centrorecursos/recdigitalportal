package pe.gob.bnp.recdigitalportal.start.dto;

import java.util.List;

import pe.gob.bnp.recdigitalportal.start.dto.ButtonDetailResponse;

public class SliderDetailResponse {
	
	private String id;
	private String title;
	private String subtitle;
	private List<ButtonDetailResponse> buttons;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}	
	public List<ButtonDetailResponse> getButtons() {
		return buttons;
	}
	public void setButtons(List<ButtonDetailResponse> buttons) {
		this.buttons = buttons;
	}

}
