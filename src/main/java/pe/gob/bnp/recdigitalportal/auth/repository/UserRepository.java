package pe.gob.bnp.recdigitalportal.auth.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigitalportal.auth.dto.Credential;
import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.repository.BaseRepository;

@Repository
public interface UserRepository extends BaseRepository<User>{	

	public ResponseTransaction login(Credential credential);
	
	public RecoveryResponseTransaction recoveryPassword(String token,String email);
	
	//public ResponseTransaction searchUser(String token);
	
	public RecoveryResponseTransaction persistCuenta(User entity);
	
	public ResponseTransaction updateToken(User entity);
	
	public ResponseTransaction read(Long id);
	
}
