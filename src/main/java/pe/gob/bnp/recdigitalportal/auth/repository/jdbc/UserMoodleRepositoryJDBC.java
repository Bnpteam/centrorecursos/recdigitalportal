package pe.gob.bnp.recdigitalportal.auth.repository.jdbc;

//import java.sql.CallableStatement;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Repository;

//import pe.gob.bnp.recdigitalportal.auth.model.User;
//import pe.gob.bnp.recdigitalportal.auth.repository.UserMoodleRepository;
//import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
//import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
//import pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc.BaseJDBCOperation;

//@Repository
public class UserMoodleRepositoryJDBC {//extends BaseJDBCOperation implements UserMoodleRepository{
	
	/*private static final Logger logger = LoggerFactory.getLogger(UserMoodleRepositoryJDBC.class);	

	private String messageSuccess="Transacción exitosa.";
	
	
	@Override 
	public ResponseTransaction persist(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call SP_SIGNUP_USER(?,?,?,?)}";/*4*/
     //   ResponseTransaction response = new ResponseTransaction();
	//	CallableStatement cstm = null;
	//	ResultSet rs = null;
	//	try {
	//		cn = this.getMysqlConnection();
	/*		cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString(1, Utility.getString(entity.getUserEmail()));
			cstm.setString(2, Utility.getString(entity.getUserPassword()));
			cstm.setString(3,Utility.getString(entity.getUserName()));	
			cstm.setString(4,Utility.getString(entity.getUserSurname()));									
		
			cstm.execute();
			response.setCodeResponse("0000");
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("SP_SIGNUP_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}


	@Override
	public ResponseTransaction updatePassword(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call SP_UPDATE_PASSWORD(?,?)}";/*2*/
      //  ResponseTransaction response = new ResponseTransaction();
	//	CallableStatement cstm = null;
	//	ResultSet rs = null;
	//	try {
	//		cn = this.getMysqlConnection();
	//		cn.setAutoCommit(false);
	//		cstm = cn.prepareCall(dBTransaction);
			
	//		cstm.setString(1, Utility.getString(entity.getUserEmail()));
	//		cstm.setString(2, Utility.getString(entity.getUserPassword()));
								
		
	//		cstm.execute();
	//		response.setCodeResponse("0000");
	//		if (response.getCodeResponse().equals("0000")) {
	//			cn.commit();
	//			response.setResponse(messageSuccess);
	//		}else {
	//			cn.rollback();				
	//		}
	//	}catch(SQLException e) {
	//		logger.error("SP_UPDATE_PASSWORD: "+e.getMessage());
	//		response.setCodeResponse(String.valueOf(e.getErrorCode()));			
	//		response.setResponse(e.getMessage());
	//		try {
	//			cn.rollback();
	//		} catch (SQLException e1) {
	//			e1.printStackTrace();
	//			response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
	//			response.setResponse(e1.getMessage());
	//		}
	//	}finally {
	//		this.closeSqlConnections(rs, cstm);
	//		this.closeConnection(cn);	
	//	}
	//	return response;
	//}
	
	/*@Override
	public ResponseTransaction update(User entity,String oldEmail) {
		Connection cn = null;
        String dBTransaction = "{ Call SP_UPDATE_USER(?,?,?,?)}";/*4*/
       // ResponseTransaction response = new ResponseTransaction();
	//	CallableStatement cstm = null;
	//	ResultSet rs = null;
	/*	try {
			cn = this.getMysqlConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString(1, Utility.getString(entity.getUserName()));
			cstm.setString(2, Utility.getString(entity.getUserSurname()));
			cstm.setString(3, Utility.getString(oldEmail));
			cstm.setString(4, Utility.getString(entity.getUserEmail()));
								
		
			cstm.execute();
			response.setCodeResponse("0000");
			if (response.getCodeResponse().equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("SP_UPDATE_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}


	/*@Override
	public ResponseTransaction update(User entity) {
		// TODO Auto-generated method stub
		return null;
	}
	 */

}
