package pe.gob.bnp.recdigitalportal.auth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.auth.dto.UserDto;
import pe.gob.bnp.recdigitalportal.auth.dto.UserPublicResponse;
import pe.gob.bnp.recdigitalportal.auth.dto.UserxTokenDto;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;
import pe.gob.bnp.recdigitalportal.auth.dto.Credential;
import pe.gob.bnp.recdigitalportal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.recdigitalportal.auth.service.AuthService;
import pe.gob.bnp.recdigitalportal.auth.service.EmailService;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

@RestController
@RequestMapping("/api")
@Api(value = "/api/auth")
@CrossOrigin(origins = "*")
public class AuthController {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);	
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	RecoveryResponseHandler recoveryResponseHandler;
	
	@Autowired
	AuthService authService;
	
	@Autowired
	EmailService  emailService;	

	/*
	 * login
	 */
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de alumno, debe estar habilitado", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> login(
			@ApiParam(value = "objeto contiene el email y password ingresado por el usuario", required = true) @RequestBody Credential credential) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = authService.login(credential);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("login IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("login Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST,path="/signup", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar alumnos", response= RecoveryResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> signup(
			@ApiParam(value = "Estructura JSON de alumnos", required = true)
			@RequestBody UserDto userDto) throws Exception {
		RecoveryResponseTransaction response=new RecoveryResponseTransaction();
		try {
			response = authService.signup(userDto);
			if(response!=null && response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				 emailService.sendHTMLEmailRegisterUser(userDto,response.getName(),response.getRecoveryUrl());
			}			
			 
			return this.recoveryResponseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("signup IllegalArgumentException: "+ex.getMessage());
			return this.recoveryResponseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("signup Throwable: "+ex.getMessage());
			return this.recoveryResponseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.PUT,path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar alumno BNP", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de alumno BNP", required = true)
			@PathVariable Long id, @RequestBody UserDto userDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = authService.update(id,userDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	

	@RequestMapping(method = RequestMethod.PUT, path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar clave alumno BNP", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "PUT")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> updatePassword(	
			@RequestParam(value = "token", defaultValue = "", required=true) String  token, @RequestBody UserxTokenDto userxTokenDto){
		
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = authService.updatexToken(token,userxTokenDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+token+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+token+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}

	}	
	
	@RequestMapping(method = RequestMethod.POST, path = "/forgotpassword", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para nueva clave", response= RecoveryResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> forgotPassword(@RequestBody RecoveryPasswordRequest recoveryPassword){
		RecoveryResponseTransaction response=new RecoveryResponseTransaction();
		try {
			response = authService.forgotPassword(recoveryPassword);
			logger.info("response valor:"+response!=null?response.getCodeResponse():"vacio");
			if(response!=null && response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				 emailService.sendHTMLEmailRecoveryPassword(recoveryPassword,response.getName(),response.getRecoveryUrl());
			}else {
				logger.info("response null o diferente de 0000"+response!=null?response.getCodeResponse():"vacio");
			}
			
			return this.recoveryResponseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("forgotpassword IllegalArgumentException: "+ex.getMessage());
			return this.recoveryResponseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("forgotpassword Throwable: "+ex.getMessage());
			return this.recoveryResponseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer alumno BNP", response= UserPublicResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id Alumno BNP", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = authService.get(id);
			if (response == null || response.getList().size()==0) {
				logger.info("/user: id:"+id+ "No se encontró datos.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/user/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	

}
