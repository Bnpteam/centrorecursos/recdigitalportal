package pe.gob.bnp.recdigitalportal.auth.dto;

import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;

public class UserxTokenDto {
	
	private String password;
	
	
	public UserxTokenDto() {
		super();
		this.password= Constants.EMPTY_STRING;		
	}	

	public UserxTokenDto(User user) {
		super();
		this.password= Utility.getString(user.getUserPassword());
		
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

}
