package pe.gob.bnp.recdigitalportal.auth.model;

import java.util.Date;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;
import pe.gob.bnp.recdigitalportal.utilitary.model.BaseEntity;

public class User extends BaseEntity{
	
	private String userName;
	private String userSurname;	
	private String userEmail;
	private String userPassword;
	private Date   birthDate;
	private String tokenRecoveryPassword;
	private String userPosition;
	private String userLibrary;
	private String codeDepartment;
	private String codeProvince;
	private String codeDistrict;
	private String working;

	public User() {
		super();
		this.userName = Constants.EMPTY_STRING;
		this.userSurname = Constants.EMPTY_STRING;
		
		this.userEmail= Constants.EMPTY_STRING;
		this.userPassword= Constants.EMPTY_STRING;
		this.birthDate= new Date();
		this.tokenRecoveryPassword= Constants.EMPTY_STRING;
		
		this.userPosition= Constants.EMPTY_STRING;
		this.userLibrary= Constants.EMPTY_STRING;
		this.codeDepartment= Constants.EMPTY_STRING;
		this.codeProvince= Constants.EMPTY_STRING;
		this.codeDistrict= Constants.EMPTY_STRING;
		this.working= Constants.EMPTY_STRING;
	}
	
	
	public String getWorking() {
		return working;
	}


	public void setWorking(String working) {
		this.working = working;
	}

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserSurname() {
		return userSurname;
	}


	public void setUserSurname(String userSurname) {
		this.userSurname = userSurname;
	}


	public String getUserEmail() {
		return userEmail;
	}


	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}


	public String getUserPassword() {
		return userPassword;
	}


	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}


	public Date getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getTokenRecoveryPassword() {
		return tokenRecoveryPassword;
	}


	public void setTokenRecoveryPassword(String tokenRecoveryPassword) {
		this.tokenRecoveryPassword = tokenRecoveryPassword;
	}


	public String getUserPosition() {
		return userPosition;
	}


	public void setUserPosition(String userPosition) {
		this.userPosition = userPosition;
	}


	public String getUserLibrary() {
		return userLibrary;
	}


	public void setUserLibrary(String userLibrary) {
		this.userLibrary = userLibrary;
	}


	public String getCodeDepartment() {
		return codeDepartment;
	}


	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}


	public String getCodeProvince() {
		return codeProvince;
	}


	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}


	public String getCodeDistrict() {
		return codeDistrict;
	}


	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}
	
}
