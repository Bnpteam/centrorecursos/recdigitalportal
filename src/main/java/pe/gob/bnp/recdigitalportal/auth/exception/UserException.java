package pe.gob.bnp.recdigitalportal.auth.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

public class UserException {
	
	private static final Logger logger = LoggerFactory.getLogger(UserException.class);
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";	
	public static final  String error0002Login ="El email ingresado no existe.";
	public static final  String error0003Login ="El usuario esta inactivo.";
	public static final  String error0004Login ="El password ingresado es incorrecto.";
	
	public static final  String error0003Update ="El usuario no existe o esta inactivo.";	
	
	public static final  String error0001 ="El email del Usuario ya se encuentra registrado.";
	public static final  String error0002Token ="El token ingresado no existe.";
	public static final  String error0003Token ="El token ingresado ha expirado.";
	public static final  String errorUserUpdate0003="El usuario no se encontró o esta inactivo.";
	
	public static ResponseTransaction setMessageResponseLogin(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Login);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Login);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004Login);
		}


		return response;
	} 
	
	
	public static RecoveryResponseTransaction setMessageResponseSave(RecoveryResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			logger.error(error9999);
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			logger.error(error0001);
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			logger.error(error0003Update);
			response.setResponse(error0003Update);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseTokenUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Token);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Token);
		}	

		return response;
	}
	
	public static RecoveryResponseTransaction setMessageResponseRecoveryPassword(RecoveryResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			logger.error(error9999);
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			logger.error(error0002Login);
			response.setResponse(error0002Login);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			logger.error(error0003Login);
			response.setResponse(error0003Login);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUser(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		

		return response;
	}
	
	

}
