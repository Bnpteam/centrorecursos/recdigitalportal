package pe.gob.bnp.recdigitalportal.auth.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.recdigitalportal.auth.dto.UserDto;
import pe.gob.bnp.recdigitalportal.auth.dto.UserxTokenDto;
import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;

@Component
public class UserMapper {
	
	public User reverseMapperSave(UserDto userDto) {
		User user = new User();	
		
		user.setUserName(userDto.getName());
		user.setUserSurname(userDto.getSurname());
		user.setUserEmail(userDto.getEmail());
		user.setUserPassword(userDto.getPassword());		
		user.setBirthDate(Utility.parseStringToDateTwo(userDto.getBirthDate()));	
		user.setWorking(userDto.getWorking());
		
		user.setUserPosition(userDto.getPosition());
		user.setUserLibrary(userDto.getLibrary());
		user.setCodeDepartment(userDto.getCodeDepartment());
		user.setCodeProvince(userDto.getCodeProvince());
		user.setCodeDistrict(userDto.getCodeDistrict());
		
		
		return user;
	}
	
	public User reverseMapperTokenUpdate(String token,UserxTokenDto userDto) {
		User user = new User();	
		
		user.setTokenRecoveryPassword(token);	
		user.setUserPassword(userDto.getPassword());			
		
		return user;
	}
	
	public User reverseMapperUpdate(Long id,UserDto userDto) {
		User user = new User();	
		
		userDto.setId(Utility.parseLongToString(id));
		user.setId(Utility.parseStringToLong(userDto.getId()));
		user.setUserName(userDto.getName());
		user.setUserSurname(userDto.getSurname());
		user.setUserEmail(userDto.getEmail());	
		user.setBirthDate(Utility.parseStringToDateTwo(userDto.getBirthDate()));	
		user.setWorking(userDto.getWorking());
		
		user.setUserPosition(userDto.getPosition());
		user.setUserLibrary(userDto.getLibrary());
		user.setCodeDepartment(userDto.getCodeDepartment());
		user.setCodeProvince(userDto.getCodeProvince());
		user.setCodeDistrict(userDto.getCodeDistrict());		
		
		return user;
	}
	

}
