package pe.gob.bnp.recdigitalportal.auth.service;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigitalportal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.recdigitalportal.auth.dto.UserDto;
import pe.gob.bnp.recdigitalportal.utilitary.common.MailContentBuilder;

@Service
public class EmailService {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	private JavaMailSenderImpl emailSender;

	private MailContentBuilder mailContentBuilder;

	@Autowired
	public EmailService(JavaMailSenderImpl emailSender, MailContentBuilder mailContentBuilder) {
		this.emailSender = emailSender;
		this.mailContentBuilder = mailContentBuilder;
	}
	
	public void sendHTMLEmailRegisterUser(UserDto userDto,String userFullName, String urlCuenta) {
		try {
			String subject="Creación de Cuenta - Centro de Recursos Digitales BNP";
			String fromEmail="notificaciones@bnp.gob.pe";
			String toEmail=userDto.getEmail().trim().toString();
			
			int year = LocalDate.now().getYear();
			String anio=year+"";
			
			
			MimeMessagePreparator messagePreparator = mimeMessage -> {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(fromEmail);
				messageHelper.setTo(toEmail);
				messageHelper.setSubject(subject);				
				String content = this.mailContentBuilder.buildTemplateRegisterUser(userFullName,urlCuenta,anio);
				messageHelper.setText(content, true);
			};
			this.emailSender.setDefaultEncoding(StandardCharsets.UTF_8.name());
			this.emailSender.send(messagePreparator);
			
		} catch (MailException e) {
			logger.error("sendHTMLEmailRegisterUser: "+e.getMessage());
		}
		
	}
	
	public void sendHTMLEmailRecoveryPassword(RecoveryPasswordRequest recoveryPassword,String userFullName,String urlRecoveryPassword) {
		try {
			String subject="Recuperación de Contraseña - Centro de Recursos Digitales BNP";
			String fromEmail="notificaciones@bnp.gob.pe";
			String toEmail=recoveryPassword.getEmail().trim().toString();
			
			int year = LocalDate.now().getYear();
			String anio=year+"";
			
			
			MimeMessagePreparator messagePreparator = mimeMessage -> {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(fromEmail);
				messageHelper.setTo(toEmail);
				messageHelper.setSubject(subject);				
				String content = this.mailContentBuilder.buildTemplateRecoveryPassword(userFullName,urlRecoveryPassword,anio);
				messageHelper.setText(content, true);
			};
			
			this.emailSender.setDefaultEncoding(StandardCharsets.UTF_8.name());
			logger.info("inicio enviando correo...");
			this.emailSender.send(messagePreparator);
			logger.info("fin enviando correo...");
			
		} catch (MailException e) {
			logger.error("sendHTMLEmailRecoveryPassword: "+e.getMessage());
		} catch (Exception ex) {
			logger.error("exception email: "+ex.getMessage());
		}
		
	}
	
	
	public void sendHTMLEmailAulaVirtual(String userEmail,String userFullName,String course,String urlEnroll) {
		try {
			String subject="Matrícula Aula Virtual - Centro de Recursos Digitales BNP";
			String fromEmail="notificaciones@bnp.gob.pe";
			String toEmail=userEmail.trim().toString();
			
			int year = LocalDate.now().getYear();
			String anio=year+"";
			
			
			MimeMessagePreparator messagePreparator = mimeMessage -> {
				MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
				messageHelper.setFrom(fromEmail);
				messageHelper.setTo(toEmail);
				messageHelper.setSubject(subject);				
				String content = this.mailContentBuilder.buildTemplateEnroll(userFullName,urlEnroll,course,anio);
				messageHelper.setText(content, true);
			};
			this.emailSender.setDefaultEncoding(StandardCharsets.UTF_8.name());
			this.emailSender.send(messagePreparator);
			
		} catch (MailException e) {
			logger.error("sendHTMLEmailAulaVirtual: "+e.getMessage());
		}
		
	}


}
