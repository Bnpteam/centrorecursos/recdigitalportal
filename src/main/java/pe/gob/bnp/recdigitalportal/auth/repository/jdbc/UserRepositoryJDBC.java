package pe.gob.bnp.recdigitalportal.auth.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigitalportal.auth.dto.Credential;
import pe.gob.bnp.recdigitalportal.auth.dto.LoginResponse;
import pe.gob.bnp.recdigitalportal.auth.dto.UserPublicResponse;
import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.auth.repository.UserRepository;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class UserRepositoryJDBC extends BaseJDBCOperation implements UserRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryJDBC.class);	

	private String messageSuccess="Transacción exitosa.";
	
	//@Autowired
	//UserMoodleRepository userMoodleRepository;
	
	
	@Override
	public ResponseTransaction login(Credential credential) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PUBLIC_LOGIN(?,?,?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_EMAIL", credential.getEmail());
			cstm.setString("P_PASSWORD", credential.getPassword());
			cstm.setString("P_RED_SOCIAL",(Utility.parseStringToInt(credential.getFlagRedSocial())).toString());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				cn.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					LoginResponse userResponse = new LoginResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setSurname(Utility.getString(rs.getString("USER_SURNAME")));
					userResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				cn.rollback();
			}
		}catch(SQLException e) {
			logger.error("SP_PUBLIC_LOGIN: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}


	@Override
	public RecoveryResponseTransaction persistCuenta(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PERSIST_PUBLIC_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*15*/
        RecoveryResponseTransaction response = new RecoveryResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_SURNAME", Utility.getString(entity.getUserSurname()));
			cstm.setString("P_EMAIL",Utility.getString(entity.getUserEmail()));	
			cstm.setString("P_PASSWORD",Utility.getString(entity.getUserPassword()));				
			//cstm.setDate("P_BIRTHDATE",Utility.getSQLDate(entity.getBirthDate()));	
			cstm.setDate("P_BIRTHDATE",Utility.parseStringToSQLDateTwo(Utility.parseDateToStringTwo(entity.getBirthDate())));
			cstm.setString("P_WORKING",Utility.getString(entity.getWorking()));	
			cstm.setString("P_POSITION",Utility.getString(entity.getUserPosition()));		
			cstm.setString("P_LIBRARY",Utility.getString(entity.getUserLibrary()));	
			cstm.setString("P_CODEDEPARTMENT",Utility.getString(entity.getCodeDepartment()));		
			cstm.setString("P_CODEPROVINCE",Utility.getString(entity.getCodeProvince()));	
			cstm.setString("P_CODEDISTRICT",Utility.getString(entity.getCodeDistrict()));	
					
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_USER_NAME", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_USER_URL", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				//ResponseTransaction responseMoodle = new ResponseTransaction();
				//responseMoodle=userMoodleRepository.persist(entity);
				
				//if(responseMoodle.getCodeResponse().equalsIgnoreCase("0000")) {
					cn.commit();
				//}else {
				//	cn.rollback();	
				//	response.setCodeResponse(responseMoodle.getCodeResponse());
				//	response.setResponse("Error Moodle:"+responseMoodle.getResponse());
				//	return response;
				//}			
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				String name=(String) (cstm.getObject("S_USER_NAME"));
				String recoveryUrl= (String) (cstm.getObject("S_USER_URL"));
				response.setId(String.valueOf(id));
				response.setName(String.valueOf(name));
				response.setRecoveryUrl(String.valueOf(recoveryUrl));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_PERSIST_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public RecoveryResponseTransaction recoveryPassword(String tokenPassword,String email) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_RECOVERY_PASSWORD(?,?,?,?,?,?)}";
        RecoveryResponseTransaction response = new RecoveryResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setString("P_TOKEN", Utility.getString(tokenPassword));
			cstm.setString("P_EMAIL", Utility.getString(email));					
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_USER_NAME", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_USER_URL", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			logger.info("codigoResultado:"+codigoResultado);
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {				
				cn.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				String name=(String) (cstm.getObject("S_USER_NAME"));
				String recoveryUrl= (String) (cstm.getObject("S_USER_URL"));
				response.setId(String.valueOf(id));
				response.setName(String.valueOf(name));
				response.setRecoveryUrl(String.valueOf(recoveryUrl));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_RECOVERY_PASSWORD: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	
	/*@Override
	public ResponseTransaction searchUser(String token) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_GET_PUBLIC_USER(?,?,?)}";
        
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_TOKEN", token);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					UserPublicResponse userPublicResponse = new UserPublicResponse();
					userPublicResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userPublicResponse.setName(Utility.getString(rs.getString("USER_FULLNAME")));
					userPublicResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userPublicResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					userPublicResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userPublicResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					usersResponse.add(userPublicResponse);
				}	
				response.setList(usersResponse);					
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_GET_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}*/


	@Override
	public ResponseTransaction updateToken(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_TOKEN_PUBLIC_USER(?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("P_TOKEN", entity.getTokenRecoveryPassword());
			cstm.setString("P_PASSWORD", Utility.getString(entity.getUserPassword()));						
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_USER_EMAIL",OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				//ResponseTransaction responseMoodle = new ResponseTransaction();
				//String userEmail = (String) cstm.getObject("S_USER_EMAIL");
				//entity.setUserEmail(userEmail);
				//responseMoodle=userMoodleRepository.updatePassword(entity);
				
				//if(responseMoodle.getCodeResponse().equalsIgnoreCase("0000")) {
					cn.commit();
				//}else {
					//cn.rollback();	
					//response.setCodeResponse(responseMoodle.getCodeResponse());
					//response.setResponse("Error Moodle:"+responseMoodle.getResponse());
					//return response;
				//}				
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_TOKEN_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	
	@Override
	public ResponseTransaction update(User entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_PUBLIC_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*14*/
        ResponseTransaction response = new ResponseTransaction();       
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_SURNAME", Utility.getString(entity.getUserSurname()));
			cstm.setString("P_EMAIL", Utility.getString(entity.getUserEmail()));
			cstm.setDate("P_BIRTHDATE",Utility.parseStringToSQLDateTwo(Utility.parseDateToStringTwo(entity.getBirthDate())));	
			cstm.setString("P_WORKING",Utility.getString(entity.getWorking()));
			cstm.setString("P_POSITION", Utility.getString(entity.getUserPosition()));
			cstm.setString("P_LIBRARY", Utility.getString(entity.getUserLibrary()));
			cstm.setString("P_CODEDEPARTMENT", Utility.getString(entity.getCodeDepartment()));
			cstm.setString("P_CODEPROVINCE", Utility.getString(entity.getCodeProvince()));
			cstm.setString("P_CODEDISTRICT", Utility.getString(entity.getCodeDistrict()));
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);	
			cstm.registerOutParameter("S_USER_EMAIL",OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				//ResponseTransaction responseMoodle = new ResponseTransaction();
				//String oldEmail = (String) cstm.getObject("S_USER_EMAIL");
				//responseMoodle=userMoodleRepository.update(entity,oldEmail);
				
				//if(responseMoodle.getCodeResponse().equalsIgnoreCase("0000")) {
					cn.commit();
				/*}else {
					cn.rollback();	
					response.setCodeResponse(responseMoodle.getCodeResponse());
					response.setResponse("Error Moodle:"+responseMoodle.getResponse());
					return response;
				}*/				
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}



	@Override
	public ResponseTransaction persist(User entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public ResponseTransaction read(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_AUTH.SP_GET_PUBLIC_USER(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_USER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					UserPublicResponse userPublicResponse = new UserPublicResponse();
					userPublicResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userPublicResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userPublicResponse.setSurname(Utility.getString(rs.getString("USER_SURNAME")));
					userPublicResponse.setBirthDate(Utility.parseSqlDateToStringTwo(rs.getDate("USER_BIRTHDATE")));
					userPublicResponse.setWorking(Utility.getString(rs.getString("USER_WORKING")));
					userPublicResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userPublicResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					
					userPublicResponse.setPosition(Utility.getString(rs.getString("USER_POSITION")));
					userPublicResponse.setLibrary(Utility.getString(rs.getString("USER_LIBRARY")));
					userPublicResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					userPublicResponse.setDepartment(Utility.getString(rs.getString("DEPARTMENT")));
					userPublicResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					userPublicResponse.setProvince(Utility.getString(rs.getString("PROVINCE")));
					userPublicResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					userPublicResponse.setDistrict(Utility.getString(rs.getString("DISTRICT")));
					
					userPublicResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userPublicResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					
					usersResponse.add(userPublicResponse);
				}	
				response.setList(usersResponse);			
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_GET_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}


}