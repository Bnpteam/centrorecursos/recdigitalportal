package pe.gob.bnp.recdigitalportal.auth.dto;

import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;

public class UserDto {
	
	private String id;
	private String name;
	private String surname;
	private String email;
	private String password;
	private String birthDate;
	private String position;
	private String library;
	private String working;
	private String codeDepartment;
	private String codeProvince;	
	private String codeDistrict;
	
	public UserDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.surname = Constants.EMPTY_STRING;
		this.email= Constants.EMPTY_STRING;
		this.password= Constants.EMPTY_STRING;
		this.birthDate=Constants.EMPTY_STRING;
		this.position=Constants.EMPTY_STRING;
		this.library=Constants.EMPTY_STRING;
		this.codeDepartment=Constants.EMPTY_STRING;
		this.codeProvince=Constants.EMPTY_STRING;
		this.codeDistrict=Constants.EMPTY_STRING;
		this.working=Constants.EMPTY_STRING;
		
	}	

	public UserDto(User user) {
		super();
		this.id =Utility.parseLongToString(user.getId());
		this.name =Utility.getString(user.getUserName()); 
		this.surname = Utility.getString(user.getUserSurname());
		this.email= Utility.getString(user.getUserEmail());
		this.password= Utility.getString(user.getUserPassword());
		this.birthDate=Utility.parseDateToString(user.getBirthDate());
		
		this.position =Utility.getString(user.getUserPosition()); 
		this.library =Utility.getString(user.getUserLibrary()); 
		this.codeDepartment =Utility.getString(user.getCodeDepartment()); 
		this.codeProvince =Utility.getString(user.getCodeProvince()); 
		this.codeDistrict =Utility.getString(user.getCodeDistrict()); 
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public String getWorking() {
		return working;
	}

	public void setWorking(String working) {
		this.working = working;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}	

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getLibrary() {
		return library;
	}

	public void setLibrary(String library) {
		this.library = library;
	}

	public String getCodeDepartment() {
		return codeDepartment;
	}

	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}

	public String getCodeProvince() {
		return codeProvince;
	}

	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}

	public String getCodeDistrict() {
		return codeDistrict;
	}

	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}

}
