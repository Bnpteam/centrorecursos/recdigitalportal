package pe.gob.bnp.recdigitalportal.auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.recdigitalportal.auth.dto.UserDto;
import pe.gob.bnp.recdigitalportal.auth.dto.UserxTokenDto;
import pe.gob.bnp.recdigitalportal.auth.model.User;
import pe.gob.bnp.recdigitalportal.auth.mapper.UserMapper;
import pe.gob.bnp.recdigitalportal.auth.dto.Credential;
import pe.gob.bnp.recdigitalportal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.recdigitalportal.auth.exception.UserException;
import pe.gob.bnp.recdigitalportal.auth.repository.UserRepository;
import pe.gob.bnp.recdigitalportal.auth.validation.UserValidation;
import pe.gob.bnp.recdigitalportal.utilitary.common.Notification;
import pe.gob.bnp.recdigitalportal.utilitary.common.RecoveryResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;

@Service
public class AuthService {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthService.class);	

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserMapper userMapper;

	public ResponseTransaction login(Credential credential) {

		ResponseTransaction response = new ResponseTransaction();

		Notification notification = UserValidation.validation(credential);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		response = userRepository.login(credential);

		return UserException.setMessageResponseLogin(response);
	}

	public RecoveryResponseTransaction signup(UserDto userDto) {

		Notification notification = UserValidation.validation(userDto);
		RecoveryResponseTransaction response = new RecoveryResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperSave(userDto);
		
		response = userRepository.persistCuenta(user);
		
		return UserException.setMessageResponseSave(response);

	}

	public ResponseTransaction update(Long id, UserDto userDto) {

		Notification notification = UserValidation.validationUpdate(userDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperUpdate(id, userDto);	
		
		response = userRepository.update(user);	
		
		return UserException.setMessageResponseUpdate(response);

	}
	
	public ResponseTransaction updatexToken(String token, UserxTokenDto userDto) {

		Notification notification = UserValidation.validationTokenUpdate(userDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperTokenUpdate(token, userDto);	
		
		response = userRepository.updateToken(user);	
		
		return UserException.setMessageResponseTokenUpdate(response);

	}

	public RecoveryResponseTransaction forgotPassword(RecoveryPasswordRequest recoveryPassword) {

		Notification notification = UserValidation.validation(recoveryPassword);
		RecoveryResponseTransaction response = new RecoveryResponseTransaction();

		if (notification.hasErrors()) {
			logger.info("error de validacion:"+notification.errorMessage());
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		String tokenPassword = Utility.generateRandomString(15);
		logger.info("tokenPassword:"+tokenPassword);

		response = userRepository.recoveryPassword(tokenPassword, recoveryPassword.getEmail());

		return UserException.setMessageResponseRecoveryPassword(response);

	}
	
	public ResponseTransaction get(Long id) {
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = UserValidation.validation(id);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		return userRepository.read(id);
	}
	

}
