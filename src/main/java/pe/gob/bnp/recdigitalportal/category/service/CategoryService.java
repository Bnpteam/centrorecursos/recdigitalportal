package pe.gob.bnp.recdigitalportal.category.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigitalportal.category.repository.CategoryRepository;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;


@Service
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	
	public ResponseTransaction searchAll(){
		
		return categoryRepository.list();
	}


}
