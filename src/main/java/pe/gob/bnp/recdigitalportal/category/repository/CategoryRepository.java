package pe.gob.bnp.recdigitalportal.category.repository;

import org.springframework.stereotype.Repository;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;


@Repository
public interface CategoryRepository {
	
	public ResponseTransaction list();//listSubcategory

}
