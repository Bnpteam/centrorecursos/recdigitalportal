package pe.gob.bnp.recdigitalportal.category.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class SubCategoryResponse {
	
	private String id;
	private String name;
	
	public SubCategoryResponse() {
		super();
		this.id=Constants.EMPTY_STRING;		
		this.name=Constants.EMPTY_STRING;		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
