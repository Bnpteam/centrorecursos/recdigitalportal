package pe.gob.bnp.recdigitalportal.category.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigitalportal.category.dto.CategoryResponse;
import pe.gob.bnp.recdigitalportal.category.repository.CategoryRepository;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc.BaseJDBCOperation;


@Repository
public class CategoryRepositoryJDBC extends BaseJDBCOperation implements CategoryRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(CategoryRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";
	
	
	@Override
	public ResponseTransaction list() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_CATEGORY.SP_LISTALL_CATEGORY(?,?)}";

        List<Object> categoriesResponse = new ArrayList<>();       
        ResponseTransaction response = new ResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);					
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					 CategoryResponse categoryResponse = new CategoryResponse();
					categoryResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));					
					categoryResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
				//	categoryResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));
					categoriesResponse.add(categoryResponse);
				}
				
				response.setList(categoriesResponse);
			}
		}catch(SQLException e) {
			logger.error("SP_LISTALL_CATEGORY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

}
