package pe.gob.bnp.recdigitalportal.category.controller;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.category.dto.CategoryResponse;
import pe.gob.bnp.recdigitalportal.category.service.CategoryService;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;
*/

//@RestController
//@RequestMapping("/api")
//@Api(value="/api/category")
//@CrossOrigin(origins = "*")
public class CategoryController {
	
	/*private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);	
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	CategoryService categoryService;
	
	
	@RequestMapping(method = RequestMethod.GET, path="/categories",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar categorias", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = categoryService.searchAll();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("categories EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("categories IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("categories Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	*/

}
