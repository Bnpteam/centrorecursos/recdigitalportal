package pe.gob.bnp.recdigitalportal.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import pe.gob.bnp.recdigitalportal.RecdigitalportalApplication;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RecdigitalportalApplication.class);
	}

}
