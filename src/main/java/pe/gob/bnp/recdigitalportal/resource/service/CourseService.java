package pe.gob.bnp.recdigitalportal.resource.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.recdigitalportal.resource.dto.EnrollDto;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualCourseRequest;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualResourceRequest;
import pe.gob.bnp.recdigitalportal.resource.exception.CourseException;
import pe.gob.bnp.recdigitalportal.resource.repository.CourseRepository;
import pe.gob.bnp.recdigitalportal.resource.validation.CourseValidation;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Notification;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

@Service
public class CourseService {


	@Autowired
	CourseRepository courseRepository;

	/*
	 * public ResponseTransaction updateEnroll(Long id,EnrollDto enrollDto) {
	 * 
	 * ResponseTransaction response = new ResponseTransaction();
	 * 
	 * response = courseRepository.updateEnroll(id,enrollDto.getUserId());
	 * 
	 * return UserException.setMessageResponseUpdate(response);
	 * 
	 * }
	 */

	public EnrollResponseTransaction persistEnroll(EnrollDto enrollDto) {

		EnrollResponseTransaction response = new EnrollResponseTransaction();

		Notification notification = CourseValidation.validation(enrollDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		response = courseRepository.persistEnroll(enrollDto);

		return CourseException.setMessageResponseEnroll(response);

	}

	public ResponseTransaction get(Long id,String userId) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */

		ResponseTransaction response = courseRepository.read(id,userId);
		// throw new IllegalArgumentException("errorMessage()");
		return CourseException.setMessageResponseSaveLink(response);
	}
	
	public ResponseTransaction getResource(Long id,String userId) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */

		ResponseTransaction response = courseRepository.readResource(id,userId);
		// throw new IllegalArgumentException("errorMessage()");
		return CourseException.setMessageResponseResourceDetail(response);
	}

	public ResponseTransaction searchRecommended(Long id) throws IOException {
		/*
		 * Notification notification = ButtonValidation.validation(id); if
		 * (notification.hasErrors()) { throw new
		 * IllegalArgumentException(notification.errorMessage()); }
		 */
		ResponseTransaction response = courseRepository.searchRecommended(id);

		return CourseException.setMessageResponseCourseRecommended(response);
	}

	public CourseResponseTransaction searchVirtualCourseAll(String topicId, String specialityId, String keyword,
			String order, String page, String size) {

		CourseResponseTransaction response = new CourseResponseTransaction();

		VirtualCourseRequest courseRequest = new VirtualCourseRequest();
		courseRequest.setTopicId(topicId);
		courseRequest.setSpecialityId(specialityId);
		courseRequest.setKeyword(keyword);
		courseRequest.setOrder(order);
		courseRequest.setPage(page);
		courseRequest.setSize(size);

		Notification notification = CourseValidation.validation(courseRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			// System.out.println("0077");
			return response;
		}

		response = courseRepository.list(courseRequest);

		return CourseException.setMessageResponseCourseList(response);
	}

	public ResourceResponseTransaction searchVirtualResourceAll(String categoryId, String keyword,
			String order, String page, String size) {

		ResourceResponseTransaction response = new ResourceResponseTransaction();

		VirtualResourceRequest courseRequest = new VirtualResourceRequest();
		courseRequest.setCategoryId(categoryId);
	//	courseRequest.setFormatId(formatId);
		courseRequest.setKeyword(keyword);
		courseRequest.setOrder(order);
		courseRequest.setPage(page);
		courseRequest.setSize(size);

		Notification notification = CourseValidation.validation(courseRequest);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			// System.out.println("0077");
			return response;
		}

		response = courseRepository.list(courseRequest);

		return CourseException.setMessageResponseCourseList(response);
	}

}
