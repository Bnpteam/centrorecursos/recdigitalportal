package pe.gob.bnp.recdigitalportal.resource.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class FormatResponse {
	
	private String id;
	private String name;
	private String icon;	

	public FormatResponse() {
		super();
		this.id=Constants.EMPTY_STRING;		
		this.name=Constants.EMPTY_STRING;	
		this.icon=Constants.EMPTY_STRING;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}
