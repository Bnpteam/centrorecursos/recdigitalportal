package pe.gob.bnp.recdigitalportal.resource.exception;

import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

public class CourseException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El id del recurso no esta publicado o no existe";
	public static final  String error0003 ="La categoria asociada no existe";
	public static final  String error0002CourseList="El tipo de order indicado no existe";
	public static final  String error0005CourseList="El id de la categoria no existe";
	public static final  String error0006CourseList="El id del formato no existe";
	public static final  String error0005CoursesList="El id del tema no existe";
	public static final  String error0006CoursesList="El id de la especialidad no existe";
	
	
	public static final  String error0001Enroll="El id del curso ingresado no es válido.";
	public static final  String error0002Enroll="El id del usuario ingresado no es válido.";
	public static final  String error0003Enroll="El usuario ya esta matriculado.";
	public static final  String error0004Enroll="El formato del enlace al aula virtual del curso es incorrecto.";
	
	public static EnrollResponseTransaction setMessageResponseEnroll(EnrollResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001Enroll);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Enroll);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Enroll);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004Enroll);
		}
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseSaveLink(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		return response;
	}
	
	//
	
	public static ResponseTransaction setMessageResponseResourceDetail(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		return response;
	}
	
	
	public static ResponseTransaction setMessageResponseCourseRecommended(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		return response;
	}
	
	
	public static ResourceResponseTransaction setMessageResponseCourseList(ResourceResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002CourseList);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005CourseList);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006CourseList);
		}
		
		
		
		return response;
	}
	
	public static CourseResponseTransaction setMessageResponseCourseList(CourseResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002CourseList);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005CoursesList);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006CoursesList);
		}		
		
		return response;
	}

}
