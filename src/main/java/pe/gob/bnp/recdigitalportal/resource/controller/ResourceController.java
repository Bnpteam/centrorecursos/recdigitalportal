package pe.gob.bnp.recdigitalportal.resource.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.resource.dto.ResourceRecommendedResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.ResourceResponse;
import pe.gob.bnp.recdigitalportal.resource.service.CourseService;
//import pe.gob.bnp.recdigitalportal.start.dto.ResourcePublishedResponse;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api")
@Api(value = "/api/resource")
@CrossOrigin(origins = "*")
public class ResourceController {

	private static final Logger logger = LoggerFactory.getLogger(ResourceController.class);

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	ResourceResponseHandler resourceResponseHandler;

	@Autowired
	CourseService courseService;
	
	@RequestMapping(method = RequestMethod.GET, path="/resource/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "detalle recurso seleccionado BNP", response= ResourceResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id recurso BNP", required = true)
			@PathVariable Long id,
			@RequestParam(value = "userId", defaultValue = "0", required=true) String  userId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = courseService.getResource(id,userId);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("resource/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("resource/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("resource/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/virtualresources", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar recursos virtuales", response = ResourceResponseTransaction.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchVirtualResourceAll(	
			@RequestParam(value = "categoryId", defaultValue = "", required=true) String  categoryId,
			//@RequestParam(value = "formatId", defaultValue = "", required=true) String  formatId,
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword,
			@RequestParam(value = "order", defaultValue = "", required=true) String  order,
			@RequestParam(value = "page",  defaultValue = "", required=true) String  page,
			@RequestParam(value = "size",  defaultValue = "", required=true) String  size){
		ResourceResponseTransaction response = new ResourceResponseTransaction();
		try {
			//response = courseService.searchVirtualResourceAll(categoryId,formatId,keyword,order,page,size);
			response = courseService.searchVirtualResourceAll(categoryId,keyword,order,page,size);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.resourceResponseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.resourceResponseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.resourceResponseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("virtualresources EntityNotFoundResultException: "+ex.getMessage());
			return this.resourceResponseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("virtualresources IllegalArgumentException: "+ex.getMessage());
			return this.resourceResponseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("virtualresources Throwable: "+ex.getMessage());
			return this.resourceResponseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	@RequestMapping(method = RequestMethod.GET,  path="/recommended/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar recursos recomendados", response = ResourceRecommendedResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ 
		    @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), 
	})
	public ResponseEntity<Object> searchRecommendedAll(	
			@ApiParam(value = "id recurso BNP", required = true)
			@PathVariable Long id){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = courseService.searchRecommended(id);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("recommended/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("recommended/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("recommended/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	

}
