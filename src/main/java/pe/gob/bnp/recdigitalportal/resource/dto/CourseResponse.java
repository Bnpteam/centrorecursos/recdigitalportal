package pe.gob.bnp.recdigitalportal.resource.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.recdigitalportal.resource.dto.CourseDetailResponse;
import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class CourseResponse {
	
	private String id;
	private String title;		
	private String link;
	private String linkStarted;	
	//private String subscriptionId;
	//private String subscription;
	private String linkCar;	
	private String linkBanner;
	private String duration;
	private String rating;
	private String enroll;
	private List<CourseDetailResponse> details;
	
	public CourseResponse() {
		super();
		this.id=Constants.EMPTY_STRING;
		this.title=Constants.EMPTY_STRING;	
		this.link=Constants.EMPTY_STRING;
		this.linkStarted=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
		//this.subscription=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;		
		this.linkBanner=Constants.EMPTY_STRING;
		this.duration=Constants.EMPTY_STRING;
		this.rating=Constants.EMPTY_STRING;	
		this.enroll=Constants.EMPTY_STRING;	
		this.details=new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLinkStarted() {
		return linkStarted;
	}

	public void setLinkStarted(String linkStarted) {
		this.linkStarted = linkStarted;
	}	
	
	/*public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}*/

	public String getLinkCar() {
		return linkCar;
	}

	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getLinkBanner() {
		return linkBanner;
	}

	public void setLinkBanner(String linkBanner) {
		this.linkBanner = linkBanner;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getEnroll() {
		return enroll;
	}

	public void setEnroll(String enroll) {
		this.enroll = enroll;
	}

	public List<CourseDetailResponse> getDetails() {
		return details;
	}

	public void setDetails(List<CourseDetailResponse> details) {
		this.details = details;
	}
	
}
