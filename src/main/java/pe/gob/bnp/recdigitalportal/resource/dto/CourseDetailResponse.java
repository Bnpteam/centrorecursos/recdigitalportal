package pe.gob.bnp.recdigitalportal.resource.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class CourseDetailResponse {
	
	private String title;
	
	private String description;
	
	public CourseDetailResponse() {
		super();
		this.title=Constants.EMPTY_STRING;
		this.description=Constants.EMPTY_STRING;
	
	}	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
