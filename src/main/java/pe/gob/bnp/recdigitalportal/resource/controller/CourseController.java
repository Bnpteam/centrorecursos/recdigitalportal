package pe.gob.bnp.recdigitalportal.resource.controller;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.recdigitalportal.auth.service.EmailService;
import pe.gob.bnp.recdigitalportal.resource.dto.CourseResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.EnrollDto;
import pe.gob.bnp.recdigitalportal.resource.service.CourseService;
import pe.gob.bnp.recdigitalportal.start.dto.ResourcePublishedResponse;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseHandler;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.exception.EntityNotFoundResultException;
*/

//@RestController
//@RequestMapping("/api")
//@Api(value = "/api/course")
//@CrossOrigin(origins = "*")
public class CourseController {
	
	//private static final Logger logger = LoggerFactory.getLogger(CourseController.class);	
	
	//@Autowired
	//ResponseHandler responseHandler;
	
	//@Autowired
	//ResourceResponseHandler resourceResponseHandler;
	
	//@Autowired
	//CourseResponseHandler courseResponseHandler;
	
	//@Autowired
	//EnrollResponseHandler enrollResponseHandler;

	//@Autowired
	//CourseService courseService;
	
	//@Autowired
	//EmailService  emailService;	
	
	
	/*@RequestMapping(method = RequestMethod.GET, path="/course/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "detalle curso seleccionado BNP", response= CourseResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id curso BNP", required = true)
			@PathVariable Long id,
			@RequestParam(value = "userId", defaultValue = "0", required=true) String  userId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = courseService.get(id,userId);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("course/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("course/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("course/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, path = "/virtualcourses", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar cursos virtuales", response = CourseResponseTransaction.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchVirtualCourseAll(	
			@RequestParam(value = "topicId", defaultValue = "", required=true) String  topicId,
			@RequestParam(value = "specialityId", defaultValue = "", required=true) String  specialityId,
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword,
			@RequestParam(value = "order", defaultValue = "", required=true) String  order,
			@RequestParam(value = "page",  defaultValue = "", required=true) String  page,
			@RequestParam(value = "size",  defaultValue = "", required=true) String  size){
		CourseResponseTransaction response = new CourseResponseTransaction();
		try {
			response = courseService.searchVirtualCourseAll(topicId,specialityId,keyword,order,page,size);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.courseResponseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.courseResponseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.courseResponseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("virtualcourses EntityNotFoundResultException: "+ex.getMessage());
			return this.courseResponseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("virtualcourses IllegalArgumentException: "+ex.getMessage());
			return this.courseResponseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("virtualcourses Throwable: "+ex.getMessage());
			return this.courseResponseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	
	/*@RequestMapping(method = RequestMethod.PUT,path="/enrollment/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "matricula aula virtual BNP", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> updateEnroll(
			@ApiParam(value = "Estructura JSON iniciar aula virtual BNP", required = true)
			@PathVariable Long id, @RequestBody EnrollDto enrollDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			response = courseService.updateEnroll(id,enrollDto);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/enrollment/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/enrollment/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}*/
	
	/*
	 * enrollment
	 */
	/*@RequestMapping(method = RequestMethod.POST, path = "/enrollment", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Matricula Aula Virtual BNP", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> registerEnroll(
			@ApiParam(value = "objeto contiene el curso y usuario", required = true) @RequestBody EnrollDto enrollDto) {
		EnrollResponseTransaction response = new EnrollResponseTransaction();
		try {
			response = courseService.persistEnroll(enrollDto);
			
			if(response!=null && response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				 emailService.sendHTMLEmailAulaVirtual(response.getEmail(),response.getName(),response.getCourse(),response.getEnrollUrl());
			}	
			return this.enrollResponseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("enrollment IllegalArgumentException: "+ex.getMessage());
			return this.enrollResponseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("enrollment Throwable: "+ex.getMessage());
			return this.enrollResponseHandler.getAppExceptionResponse(response, ex);
		}

	}*/

}
