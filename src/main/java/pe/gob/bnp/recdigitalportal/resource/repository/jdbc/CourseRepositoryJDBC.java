package pe.gob.bnp.recdigitalportal.resource.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.recdigitalportal.category.dto.CategoryResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.CourseDetailResponse;
//import pe.gob.bnp.recdigitalportal.resource.dto.CourseRecommendedResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.CourseResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.EnrollDto;
import pe.gob.bnp.recdigitalportal.resource.dto.ResourceRecommendedResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.ResourceResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.SpecialityResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.TopicResponse;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualCourseRequest;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualResourceRequest;
//import pe.gob.bnp.recdigitalportal.resource.repository.CourseMoodleRepository;
import pe.gob.bnp.recdigitalportal.resource.repository.CourseRepository;
import pe.gob.bnp.recdigitalportal.start.dto.CoursePublishedResponse;
import pe.gob.bnp.recdigitalportal.start.dto.ResourcePublishedResponse;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;
import pe.gob.bnp.recdigitalportal.utilitary.repository.jdbc.BaseJDBCOperation;

@Repository
public class CourseRepositoryJDBC extends BaseJDBCOperation implements CourseRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(CourseRepositoryJDBC.class);
	
	private String messageSuccess="Transacción exitosa.";	
	
	//@Autowired
	//CourseMoodleRepository courseMoodleRepository;
	
	
	@Override
	public EnrollResponseTransaction persistEnroll(EnrollDto enrollDto) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_PERSIST_ENROLL(?,?,?,?,?,?,?,?,?)}";/*9*/
        EnrollResponseTransaction response = new EnrollResponseTransaction();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_COURSE_ID", Utility.parseStringToLong(Utility.getString(enrollDto.getCourseId())));
			cstm.setLong("P_USER_ID",  Utility.parseStringToLong(Utility.getString(enrollDto.getUserId())) );
			
					
			cstm.registerOutParameter("S_ENROLLMENT_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_COURSE_CODE", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_USER_EMAIL",OracleTypes.VARCHAR);	
			cstm.registerOutParameter("S_USER_NAME",OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_COURSE",OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_USER_URL",OracleTypes.VARCHAR);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);				
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")|| response.getCodeResponse().equals("0011")) {
				if(response.getCodeResponse().equals("0000")) {
					
					//ResponseTransaction responseMoodle = new ResponseTransaction();
					//String courseId = (String) cstm.getObject("S_COURSE_CODE");
					//String userEmail = (String) cstm.getObject("S_USER_EMAIL");
					//responseMoodle=courseMoodleRepository.persistEnroll(userEmail,courseId);
					
					//if(responseMoodle.getCodeResponse().equalsIgnoreCase("0000")) {
						cn.commit();
					//}else {
						//cn.rollback();	
						//response.setCodeResponse(responseMoodle.getCodeResponse());
						//response.setResponse("Error Moodle:"+responseMoodle.getResponse());
						//return response;
					//}
					
				}
				
				if(response.getCodeResponse().equals("0011")) {
					response.setCodeResponse("0000");
					cn.commit();
				}							
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_ENROLLMENT_ID"));
				String name=(String) (cstm.getObject("S_USER_NAME"));
				String email=(String) (cstm.getObject("S_USER_EMAIL"));
				String recoveryUrl= (String) (cstm.getObject("S_USER_URL"));
				String course= (String) (cstm.getObject("S_COURSE"));
				response.setId(String.valueOf(id));
				response.setName(String.valueOf(name));
				response.setEmail(String.valueOf(email));
				response.setEnrollUrl(String.valueOf(recoveryUrl));
				response.setCourse(String.valueOf(course));
				response.setResponse(messageSuccess);
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_PERSIST_ENROLL: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction read(Long id,String userId) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_GET_COURSE(?,?,?,?,?)}";
             
        CourseResponse courseResponse = new CourseResponse();
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_COURSE_ID", id);
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(userId));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);	
			cstm.registerOutParameter("S_C_CURSOR_DETAIL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					
					courseResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					courseResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));					
					courseResponse.setDuration(Utility.getString(rs.getString("RESOURCE_DURATION")));	
					courseResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					courseResponse.setLinkStarted(Utility.getString(rs.getString("STARTED_LINK")));
					//courseResponse.setSubscriptionId(Utility.getString(rs.getString("SUBSCRIPTION_ID")));
					//courseResponse.setSubscription(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					courseResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					courseResponse.setLinkBanner(Utility.getString(rs.getString("BANNER_LINK")));
					courseResponse.setRating(Utility.getString(rs.getString("RATING")));				
					courseResponse.setEnroll(Utility.getString(rs.getString("ENROLL")));	
				}
				this.closeResultSet(rs);
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_DETAIL");
				List<CourseDetailResponse> detailsDto = new ArrayList<>();
				while (rs.next()) {
					CourseDetailResponse entidad = new CourseDetailResponse();
					
					entidad.setTitle(Utility.getString(rs.getString("COURSE_DETAIL_TITLE")));
					entidad.setDescription(Utility.getString(rs.getString("COURSE_DETAIL_DES")));					
					detailsDto.add(entidad);
				}	
				
				courseResponse.setDetails(detailsDto);
				
				if(courseResponse!=null && !Utility.isEmptyOrNull(courseResponse.getTitle()) ) {
					resourcesResponse.add(courseResponse);
					
				}
				
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL_COURSE.SP_GET_COURSE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				logger.error("SP_GET_COURSE SQLException: "+e.getMessage());
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction searchRecommended(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_LIST_RECOMMENDED(?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_COURSE_ID", id);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					ResourceRecommendedResponse resourceResponse = new ResourceRecommendedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("CATEGORY_NAME")));
					//resourceResponse.setTopicId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					//resourceResponse.setTopic(Utility.getString(rs.getString("TOPIC_NAME")));
					//resourceResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					//resourceResponse.setSpeciality(Utility.getString(rs.getString("SPECIALITY_NAME")));
				//	resourceResponse.setCategoryId(Utility.getString(rs.getString("CATEGORY_ID")));
				//	resourceResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));
				//	resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
				//	resourceResponse.setSubcategory(Utility.getString(rs.getString("RESOURCE_SUBCATEGORY")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK3")));
					//resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					//resourceResponse.setRating(Utility.getString(rs.getString("RATING")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					//resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_DETAIL_COURSE.SP_LIST_RECOMMENDED: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public CourseResponseTransaction list(VirtualCourseRequest vcRequest) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_LIST_VIRTUALCOURSE(?,?,?,?,?,?,?,?,?,?)}";/*10*/

        List<Object> resourcesResponse = new ArrayList<>();
        List<Object> categoriesResponse = new ArrayList<>();
        List<Object> subcategoriesResponse = new ArrayList<>();
        CourseResponseTransaction response = new CourseResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_TOPIC_ID", Utility.parseStringToLong(vcRequest.getTopicId()));	
			cstm.setLong("P_SPECIALITY_ID", Utility.parseStringToLong(vcRequest.getSpecialityId()));	
			cstm.setString("P_KEYWORD", Utility.getString(vcRequest.getKeyword()));	
			cstm.setInt("P_ORDER", Utility.parseStringToInt(vcRequest.getOrder()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(vcRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(vcRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_TOPICS",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_SPECIALITIES",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					CoursePublishedResponse resourceResponse = new CoursePublishedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("COURSE_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("COURSE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("COURSE_DESCRIPTION")));
					resourceResponse.setTopicId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					resourceResponse.setTopic(Utility.getString(rs.getString("TOPIC_NAME")));
					resourceResponse.setSpecialityId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					resourceResponse.setSpeciality(Utility.getString(rs.getString("SPECIALITY_NAME")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setRating(Utility.getString(rs.getString("RATING")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					resourcesResponse.add(resourceResponse);
				}
				this.closeResultSet(rs);
				
				/*topics*/
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_TOPICS");
				while (rs.next()) {
					TopicResponse resourceResponse = new TopicResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("TOPIC_NAME")));					
					
					categoriesResponse.add(resourceResponse);
				}
				this.closeResultSet(rs);
				
				/*specialities*/
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_SPECIALITIES");
				while (rs.next()) {
					SpecialityResponse resourceResponse = new SpecialityResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("SPECIALITY_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("SPECIALITY_NAME")));			
					
					subcategoriesResponse.add(resourceResponse);
				}				
				
				response.setList(resourcesResponse);
				response.setTopics(categoriesResponse);
				response.setSpecialities(subcategoriesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL_COURSE.SP_LIST_VIRTUALCOURSE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResourceResponseTransaction list(VirtualResourceRequest vcRequest) {

		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_LIST_VIRTUALRESOURCE(?,?,?,?,?,?,?,?)}";/*8*/
        
        List<Object> resourcesResponse = new ArrayList<>();
        List<Object> categoriesResponse = new ArrayList<>();
      //  List<Object> subcategoriesResponse = new ArrayList<>();
        ResourceResponseTransaction response = new ResourceResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			
			cstm.setLong("P_CATEGORY_ID", Utility.parseStringToLong(vcRequest.getCategoryId()));	
			//cstm.setLong("P_FORMAT_ID", Utility.parseStringToLong(vcRequest.getFormatId()));	
			cstm.setString("P_KEYWORD", Utility.getString(vcRequest.getKeyword()));	
			cstm.setInt("P_ORDER", Utility.parseStringToInt(vcRequest.getOrder()));	
			cstm.setInt("P_PAGE", Utility.parseStringToInt(vcRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(vcRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_CATEGORIES",OracleTypes.CURSOR);
		//	cstm.registerOutParameter("S_C_CURSOR_FORMATS",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					ResourcePublishedResponse resourceResponse = new ResourcePublishedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					resourceResponse.setTitleOriginal(Utility.getString(rs.getString("RESOURCE_TITLE_ORIGINAL")));
					resourceResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));
					resourceResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					resourceResponse.setTitleBig(Utility.getString(rs.getString("RESOURCE_TITLE_BIG")));
					resourceResponse.setDescriptionBig(Utility.getString(rs.getString("RESOURCE_DESCRIPTION_BIG")));
					resourceResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					resourceResponse.setCategory(Utility.getString(rs.getString("CATEGORY_NAME")));
					resourceResponse.setFormatId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					resourceResponse.setFormat(Utility.getString(rs.getString("FORMAT_NAME")));
					//resourceResponse.setFormatIcon(Utility.getString(rs.getString("FORMAT_ICON")));
					//resourceResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));
					//resourceResponse.setSubcategoryId(Utility.getString(rs.getString("SUBCATEGORY_ID")));
					//resourceResponse.setSubcategory(Utility.getString(rs.getString("RESOURCE_SUBCATEGORY")));
					resourceResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					resourceResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					resourceResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK3")));
					resourceResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					resourceResponse.setObservation(Utility.getString(rs.getString("RESOURCE_OBSERVATION")));
					resourceResponse.setViews(Utility.getString(rs.getString("NUMBER_VIEWS")));
					resourceResponse.setLikes(Utility.getString(rs.getString("NUMBER_LIKES")));
					resourceResponse.setShares(Utility.getString(rs.getString("NUMBER_SHARES")));
					resourceResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourceResponse.setTotalRecords(Utility.getString(rs.getString("TOTAL_RECORDS")));
					
					resourcesResponse.add(resourceResponse);
				}
				this.closeResultSet(rs);
				
				/*categories*/
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_CATEGORIES");
				while (rs.next()) {
					CategoryResponse resourceResponse = new CategoryResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
					//resourceResponse.setIcon(Utility.getString(rs.getString("CATEGORY_ICON")));				
					
					categoriesResponse.add(resourceResponse);
				}
				this.closeResultSet(rs);
				
				/*formats*/
				/*rs = (ResultSet) cstm.getObject("S_C_CURSOR_FORMATS");
				while (rs.next()) {
					FormatResponse resourceResponse = new FormatResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					resourceResponse.setName(Utility.getString(rs.getString("FORMAT_NAME")));
					resourceResponse.setIcon(Utility.getString(rs.getString("FORMAT_ICON")));
					
					subcategoriesResponse.add(resourceResponse);
				}*/				
				
				response.setList(resourcesResponse);
				response.setCategories(categoriesResponse);
			//	response.setFormats(subcategoriesResponse);
				
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL_COURSE.SP_LIST_VIRTUALRESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTransaction readResource(Long id, String userId) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_DETAIL_COURSE.SP_GET_RESOURCE(?,?,?,?)}";
             
        ResourceResponse courseResponse = new ResourceResponse();
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getOracleConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("P_RESOURCE_ID", id);
			cstm.setLong("P_USER_ID", Utility.parseStringToLong(userId));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {					
					
					courseResponse.setId(Utility.parseLongToString(rs.getLong("RESOURCE_ID")));
					courseResponse.setTitle(Utility.getString(rs.getString("RESOURCE_TITLE")));	
					courseResponse.setDescription(Utility.getString(rs.getString("RESOURCE_DESCRIPTION")));
					courseResponse.setDuration(Utility.getString(rs.getString("RESOURCE_DURATION")));	
					courseResponse.setLink(Utility.getString(rs.getString("RESOURCE_LINK")));
					//courseResponse.setLinkStarted(Utility.getString(rs.getString("STARTED_LINK")));
					//courseResponse.setSubscriptionId(Utility.getString(rs.getString("SUBSCRIPTION_ID")));
					//courseResponse.setSubscription(Utility.getString(rs.getString("SUBSCRIPTION_NAME")));
					courseResponse.setExtensionId(Utility.parseLongToString(rs.getLong("RESOURCE_EXTENSION_ID")));
					courseResponse.setExtension(Utility.getString(rs.getString("RESOURCE_EXTENSION")));
					courseResponse.setCategoryId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					courseResponse.setCategory(Utility.getString(rs.getString("CATEGORY_NAME")));
					courseResponse.setFormatId(Utility.parseLongToString(rs.getLong("FORMAT_ID")));
					courseResponse.setFormat(Utility.getString(rs.getString("FORMAT_NAME")));	
					courseResponse.setFormatIcon(Utility.getString(rs.getString("FORMAT_ICON")));	
					courseResponse.setLinkCar(Utility.getString(rs.getString("CAR_LINK")));
					courseResponse.setLinkCar2(Utility.getString(rs.getString("CAR_LINK2")));
					courseResponse.setLinkCar3(Utility.getString(rs.getString("CAR_LINK3")));
					//courseResponse.setLinkBanner(Utility.getString(rs.getString("BANNER_LINK")));
					courseResponse.setRating(Utility.getString(rs.getString("RATING")));		
					courseResponse.setLikes(Utility.getString(rs.getString("NUMBER_LIKES")));	
					courseResponse.setFlagLike(Utility.getString(rs.getString("LIKES")));
					courseResponse.setPublicationDate(Utility.parseDateToString(Utility.getDate(rs.getDate("DATE_PUBLISHED"))));
					resourcesResponse.add(courseResponse);
				}

				response.setList(resourcesResponse);
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL_COURSE.SP_GET_RESOURCE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				logger.error("SP_GET_RESOURCE SQLException: "+e.getMessage());
				response.setCodeResponse(String.valueOf(e1.getErrorCode()));			
				response.setResponse(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

}
