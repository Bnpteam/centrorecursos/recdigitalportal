package pe.gob.bnp.recdigitalportal.resource.validation;

import pe.gob.bnp.recdigitalportal.resource.dto.EnrollDto;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualCourseRequest;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualResourceRequest;
import pe.gob.bnp.recdigitalportal.utilitary.common.Notification;
import pe.gob.bnp.recdigitalportal.utilitary.common.Utility;

public class CourseValidation {
	
	private static String messageCourseList = "No se encontraron datos de categoria, subcategoria, order, page o size";
	private static String messageCourseListOrder= "Se debe ingresar un tipo de order";
	private static String messageCourseListCategoria= "Se debe ingresar categoria";
	private static String messageCourseListTopic= "Se debe ingresar tema";
	private static String messageCourseListSpeciality= "Se debe ingresar especialidad";
//	private static String messageCourseListFormat= "Se debe ingresar formato";
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";
	
	private static String messageEnroll= "No se encontraron datos";
	private static String messageEnrollCourse= "Se debe ingresar el id del curso";
	private static String messageEnrollUser= "Se debe ingresar el id del usuario";
	
	
	public static Notification validation(EnrollDto enrollDto) {
		Notification notification = new Notification();

		if (enrollDto == null) {
			notification.addError(messageEnroll);
			return notification;
		}

		if (Utility.isEmptyOrNull(enrollDto.getCourseId())) {
			notification.addError(messageEnrollCourse);
		}
		
		if (Utility.isEmptyOrNull(enrollDto.getUserId())) {
			notification.addError(messageEnrollUser);
		}		
		
		return notification;
	}
	
	
	
	public static Notification validation(VirtualCourseRequest courseRequest) {
		Notification notification = new Notification();

		if (courseRequest == null) {
			notification.addError(messageCourseList);
			return notification;
		}

		if (Utility.isEmptyOrNull(courseRequest.getOrder())) {
			notification.addError(messageCourseListOrder);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getSize())) {
			notification.addError(messageCourseListSize);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getTopicId())) {
			notification.addError(messageCourseListTopic);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getSpecialityId())) {
			notification.addError(messageCourseListSpeciality);
		}

		return notification;
	}
	
	public static Notification validation(VirtualResourceRequest courseRequest) {
		Notification notification = new Notification();

		if (courseRequest == null) {
			notification.addError(messageCourseList);
			return notification;
		}

		if (Utility.isEmptyOrNull(courseRequest.getOrder())) {
			notification.addError(messageCourseListOrder);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getSize())) {
			notification.addError(messageCourseListSize);
		}
		
		if (Utility.isEmptyOrNull(courseRequest.getCategoryId())) {
			notification.addError(messageCourseListCategoria);
		}
		
	/*	if (Utility.isEmptyOrNull(courseRequest.getFormatId())) {
			notification.addError(messageCourseListFormat);
		}
*/
		return notification;
	}

}
