package pe.gob.bnp.recdigitalportal.resource.dto;

import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class EnrollDto {

	private String courseId;

	private String userId;

	public EnrollDto() {
		super();
		this.courseId = Constants.EMPTY_STRING;
		this.userId = Constants.EMPTY_STRING;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

}
