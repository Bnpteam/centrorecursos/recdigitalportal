package pe.gob.bnp.recdigitalportal.resource.dto;


import pe.gob.bnp.recdigitalportal.utilitary.common.Constants;

public class ResourceResponse {
	
	private String id;
	private String title;		
	private String link;		
	private String description;
	private String extensionId;
	private String extension;
	private String categoryId;
	private String category;	
	private String formatId;
	private String format;
	private String formatIcon;
	
	private String linkCar;	
	private String linkCar2;	
	private String linkCar3;
	private String duration;
	private String rating;
	private String flagLike;	
	private String likes;
	private String publicationDate;

	public ResourceResponse() {
		super();
		this.id=Constants.EMPTY_STRING;
		this.title=Constants.EMPTY_STRING;	
		this.link=Constants.EMPTY_STRING;
		//this.linkStarted=Constants.EMPTY_STRING;
		//this.subscriptionId=Constants.EMPTY_STRING;
		//this.subscription=Constants.EMPTY_STRING;
		this.linkCar=Constants.EMPTY_STRING;	
		this.linkCar2=Constants.EMPTY_STRING;	
		this.linkCar3=Constants.EMPTY_STRING;	
		//this.linkBanner=Constants.EMPTY_STRING;
		this.duration=Constants.EMPTY_STRING;
		this.rating=Constants.EMPTY_STRING;	
		this.flagLike=Constants.EMPTY_STRING;	
		//this.details=new ArrayList<>();
		this.likes=Constants.EMPTY_STRING;
	}
	
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String getExtensionId() {
		return extensionId;
	}


	public void setExtensionId(String extensionId) {
		this.extensionId = extensionId;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getLinkCar() {
		return linkCar;
	}

	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	public String getLinkCar2() {
		return linkCar2;
	}

	public void setLinkCar2(String linkCar2) {
		this.linkCar2 = linkCar2;
	}

	public String getLinkCar3() {
		return linkCar3;
	}

	public void setLinkCar3(String linkCar3) {
		this.linkCar3 = linkCar3;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRating() {
		return rating;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getFormatId() {
		return formatId;
	}

	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFormatIcon() {
		return formatIcon;
	}
	
	public void setFormatIcon(String formatIcon) {
		this.formatIcon = formatIcon;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getFlagLike() {
		return flagLike;
	}

	public void setFlagLike(String flagLike) {
		this.flagLike = flagLike;
	}
	
	public String getLikes() {
		return likes;
	}

	public void setLikes(String likes) {
		this.likes = likes;
	}


	public String getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}

	
}
