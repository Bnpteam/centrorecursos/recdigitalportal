package pe.gob.bnp.recdigitalportal.resource.dto;

public class CourseRecommendedResponse {
	
	private String id;	
	private String title;
	private String description;
	private String categoryId;
	private String category;	
	//private String topicId;
	//private String topic;
	//private String specialityId;
	//private String speciality;	
	//private String totalRecords;
	//private String categoryId;
	//private String icon;	
	//private String subcategoryId;
	//private String subcategory;
	private String linkCar;
	private String linkCar2;	
	private String linkCar3;
	//private String views;
	//private String rating;
	private String publicationDate;		
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLinkCar2() {
		return linkCar2;
	}
	public void setLinkCar2(String linkCar2) {
		this.linkCar2 = linkCar2;
	}
	public String getLinkCar3() {
		return linkCar3;
	}
	public void setLinkCar3(String linkCar3) {
		this.linkCar3 = linkCar3;
	}
	
	/*public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getSpecialityId() {
		return specialityId;
	}
	public void setSpecialityId(String specialityId) {
		this.specialityId = specialityId;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}*/
	/*public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}*/
	/*public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(String subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}*/
	
	public String getLinkCar() {
		return linkCar;
	}
	public void setLinkCar(String linkCar) {
		this.linkCar = linkCar;
	}
	/*public String getViews() {
		return views;
	}
	public void setViews(String views) {
		this.views = views;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}*/
	
	public String getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(String publicationDate) {
		this.publicationDate = publicationDate;
	}
	
}
