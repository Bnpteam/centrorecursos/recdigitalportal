package pe.gob.bnp.recdigitalportal.resource.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.recdigitalportal.resource.dto.EnrollDto;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualCourseRequest;
import pe.gob.bnp.recdigitalportal.resource.dto.VirtualResourceRequest;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResourceResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.CourseResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.EnrollResponseTransaction;
import pe.gob.bnp.recdigitalportal.utilitary.common.ResponseTransaction;

@Repository
public interface CourseRepository {
	
	public ResponseTransaction read(Long id,String userId);
	
	public ResponseTransaction readResource(Long id,String userId);
	
	public ResponseTransaction searchRecommended(Long id);
	
	public CourseResponseTransaction list(VirtualCourseRequest vcRequest);
	
	public ResourceResponseTransaction list(VirtualResourceRequest vcRequest);
	
	public EnrollResponseTransaction persistEnroll(EnrollDto enrollDto);

}
